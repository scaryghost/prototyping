package scaryghost.prototyping.grammar

class IdentifierGrammar extends Grammar {
    private static def PATTERN = ~'^[a-z|A-Z|_]\\w*$'

    IdentifierGrammar(Grammar parent) {
        attach(parent)
        isComplete = true
    }

    @Override
    void attach(Grammar parent) {
        parent.transitions[Transition.instance] = this
    }

    @Singleton
    private static class Transition implements scaryghost.prototyping.grammar.Transition {
        @Override
        boolean accepts(String token, State state) {
            return token ==~ PATTERN
        }
    }
}
