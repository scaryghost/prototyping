package scaryghost.prototyping.grammar

import scaryghost.prototyping.parser.UnaryOperator

@Singleton
class LeftUnaryExpressionTransition implements Transition {
    @Override
    boolean accepts(String token, State state) {
        return UnaryOperator.NAMES.containsKey(token)
    }
}
