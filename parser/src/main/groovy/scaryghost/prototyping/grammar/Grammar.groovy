package scaryghost.prototyping.grammar

abstract class Grammar {
    Map<Transition, Grammar> transitions = [:]
    def isComplete = false

    abstract void attach(Grammar parent)

    List<Grammar> iterate(List<String> tokens, State state) {
        tokens.inject([this]) {acc, t ->
            acc.collect {
                it.next(t, state)
            }.flatten()
        }
    }

    List<Grammar> next(String token, State state) {
        def trees = [this] + transitions.findAll {
            it.key instanceof EpsilonTransition
        }.collect {
            it.value
        }

        trees.collect{
            Grammar.findValidChildren(it.transitions, token, state)
        }.flatten()
    }

    static private List<Grammar> findValidChildren(transitions, token, state) {
        transitions.findAll {
            if (it.key.accepts(token, state)) {
                it.key.update(state)
                return true
            }
            return false
        }.collect {
            it.value
        }
    }
}