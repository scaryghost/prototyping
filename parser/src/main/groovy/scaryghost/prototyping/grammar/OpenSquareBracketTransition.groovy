package scaryghost.prototyping.grammar

@Singleton
class OpenSquareBracketTransition implements Transition {
    @Override
    boolean accepts(String token, State state) {
        return token == "["
    }

    @Override
    void update(State state) {
        state.stack.push("[")
    }
}