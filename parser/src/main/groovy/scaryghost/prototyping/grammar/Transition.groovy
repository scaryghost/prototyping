package scaryghost.prototyping.grammar

trait Transition {
    abstract boolean accepts(String token, State state)
    void update(State state) {

    }
}