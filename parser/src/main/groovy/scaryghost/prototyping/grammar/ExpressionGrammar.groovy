package scaryghost.prototyping.grammar

@Singleton(strict = false)
class ExpressionGrammar extends Grammar {
    ExpressionGrammar() {
        new IdentifierGrammar(this).with {
            transitions[BinaryExpressionTransition.instance] = this
            transitions[ClosedParenthesisTransition.instance] = it
            transitions[OpenSquareBracketTransition.instance] = this
            transitions[ClosedSquareBracketTransition.instance] = it
        }

        transitions[LeftUnaryExpressionTransition.instance] = this
        transitions[OpenParenthesisTransition.instance] = this
    }

    @Override
    void attach(Grammar parent) {
        parent.transitions[EpsilonTransition.instance] = this
    }
}
