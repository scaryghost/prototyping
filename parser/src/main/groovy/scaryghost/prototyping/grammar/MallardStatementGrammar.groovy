package scaryghost.prototyping.grammar

final class MallardStatementGrammar extends Grammar {
    MallardStatementGrammar() {
        transitions[EpsilonTransition.instance] = ExpressionGrammar.instance
    }

    @Override
    void attach(Grammar parent) {
        throw new IllegalStateException("Cannot attach parent to root node")
    }
}
