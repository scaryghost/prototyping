package scaryghost.prototyping.grammar

@Singleton
class ClosedSquareBracketTransition implements Transition {
    @Override
    boolean accepts(String token, State state) {
        return token == "]" && !state.stack.isEmpty() && state.stack.peek() == "["
    }

    @Override
    void update(State state) {
        state.stack.pop()
    }
}
