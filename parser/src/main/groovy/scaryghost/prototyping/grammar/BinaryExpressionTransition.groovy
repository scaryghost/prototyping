package scaryghost.prototyping.grammar

import scaryghost.prototyping.parser.BinaryOperator

@Singleton
class BinaryExpressionTransition implements Transition {
    @Override
    boolean accepts(String token, State state) {
        return BinaryOperator.NAMES.containsKey(token)
    }
}
