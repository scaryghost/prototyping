package scaryghost.prototyping.parser

trait Expression {
    List<Expression> subExpressions
    boolean isGrouped = false

    boolean isChild() {
        return subExpressions?.isEmpty() ?: true
    }
}
