package scaryghost.prototyping.parser

enum UnaryOperator {
    MINUS("-")

    public static Map<String, UnaryOperator> NAMES = UnaryOperator.inject([:]) { acc, e ->
        acc[e.repr] = e
        acc
    }

    public final String repr

    UnaryOperator(repr) {
        this.repr = repr
    }
}
