package scaryghost.prototyping.parser

class Literal implements Expression {
    String value

    Literal() {
        subExpressions = null
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Literal literal = (Literal) o

        if (isGrouped != literal.isGrouped) return false
        if (subExpressions != literal.subExpressions) return false
        if (value != literal.value) return false

        return true
    }

    int hashCode() {
        int result
        result = (value != null ? value.hashCode() : 0)
        result = 31 * result + (subExpressions != null ? subExpressions.hashCode() : 0)
        result = 31 * result + (isGrouped ? 1 : 0)
        return result
    }
}
