package scaryghost.prototyping.parser

class ExpressionBuilder {
    private Map<Expression, Expression> parents = [:] as IdentityHashMap
    Stack<State> partialExpressions = new Stack<>()

    ExpressionBuilder() {
        partialExpressions.push(new State())
    }

    Expression build() {
        if (partialExpressions.size() != 1) {
            throw new IllegalStateException("Expression is incomplete, missing ${partialExpressions.size()} closing parenthesis: ')'")
        }

        return partialExpressions.peek().root
    }

    ExpressionBuilder munch(String token) {
        switch (token) {
            case "(":
                partialExpressions.push(new State())
                break
            case ")":
                if (!partialExpressions.peek().isComplete()) {
                    throw new IllegalStateException("Inner expression is not complete")
                }

                def state = partialExpressions.pop()
                state.root.isGrouped = true

                if (partialExpressions.isEmpty()) {
                    throw new IllegalStateException("Missing a '(' somewhere")
                }

                if (partialExpressions.peek().isEmpty()) {
                    partialExpressions.pop()
                    partialExpressions.push(state)
                } else {
                    partialExpressions.peek().attachExpression(state.root)
                }
                break
            default:
                partialExpressions.peek().munch(token)
        }

        return this
    }

    private class State {
        private Expression current
        def munch = this.&whenEmpty

        boolean isComplete() {
            return munch.getMethod() == "whenCompleted"
        }

        boolean isEmpty() {
            return current == null
        }

        Expression getRoot() {
            if (!parents.containsKey(current)) {
                return current
            }

            Expression it = parents[current]
            while (parents.containsKey(it)) {
                it = parents[it]
            }
            return it
        }

        void attachExpression(Expression expr) {
            if (isComplete()) {
                throw new IllegalStateException("Expression is already complete, missing binary operator?")
            }
            current.subExpressions[-1] = expr
        }

        private void whenEmpty(String token) {
            switch (token) {
                case { BinaryOperator.NAMES.containsKey(it) }:
                    if (!UnaryOperator.NAMES.containsKey(token)) {
                        throw new IllegalArgumentException("Cannot start expression with a binary operator")
                    }
                case { UnaryOperator.NAMES.containsKey(it) }:
                    current = new UnaryExpression(operator: UnaryOperator.NAMES[token])
                    munch = this.&whenUnaryOperator
                    break
                default:
                    current = new Literal(value: token)
                    munch = this.&whenCompleted
                    break
            }
        }

        private void whenUnaryOperator(String token) {
            switch (token) {
                case { BinaryOperator.NAMES.containsKey(it) }:
                    if (!UnaryOperator.NAMES.containsKey(token)) {
                        throw new IllegalArgumentException("Cannot follow a unary operator with a binary operator")
                    }
                case { UnaryOperator.NAMES.containsKey(it) }:
                    def child = new UnaryExpression(operator: UnaryOperator.NAMES[token])
                    current.subExpressions[0] = child
                    parents[child] = current

                    current = child
                    break
                default:
                    def child = new Literal(value:token)
                    current.subExpressions[0] = child

                    munch = this.&whenCompleted
                    break
            }
        }

        private void whenBinaryOperator(String token) {
            switch (token) {
                case { BinaryOperator.NAMES.containsKey(it) }:
                    if (!UnaryOperator.NAMES.containsKey(token)) {
                        throw new IllegalArgumentException("An expression must be between two binary operators")
                    }
                case { UnaryOperator.NAMES.containsKey(it) }:
                    def child = new UnaryExpression(operator: UnaryOperator.NAMES[token])
                    parents[child] = current
                    current.subExpressions[1] = child

                    current = child
                    munch = this.&whenUnaryOperator
                    break
                default:
                    def child = new Literal(value: token)
                    current.subExpressions[1] = child

                    munch = this.&whenCompleted
                    break
            }
        }

        private void whenCompleted(String token) {
            switch (token) {
                case { BinaryOperator.NAMES.containsKey(it) }:
                    def lowerPriority = {
                        def parent = new BinaryExpression(operator: BinaryOperator.NAMES[token]).tap {
                            it.subExpressions[0] = current
                        }

                        if (parents.containsKey(current)) {
                            parents[parent] = parents[current]
                            parents[current].subExpressions[-1] = parent
                        }
                        parents[current] = parent

                        current = parent
                    }

                    switch (current) {
                        case UnaryExpression:
                            lowerPriority()
                            break
                        case BinaryExpression:
                            def newPriority = BinaryOperator.NAMES[token].priority
                            if (current.isGrouped || current.operator.priority >= newPriority) {
                                lowerPriority()
                            } else {
                                def child = new BinaryExpression(operator: BinaryOperator.NAMES[token]).tap {
                                    it.subExpressions[0] = current.subExpressions.last()
                                }

                                parents[child] = current
                                current.subExpressions[1] = child
                                current = child
                            }
                            break
                        case Literal:
                            def parent = new BinaryExpression(operator: BinaryOperator.NAMES[token]).tap {
                                it.subExpressions[0] = current
                            }
                            parents[current] = parent

                            current = parent
                            break
                        default:
                            throw new IllegalArgumentException("Unhandled expression tree type: ${current.class}")
                    }

                    munch = this.&whenBinaryOperator
                    break
                default:
                    throw new IllegalArgumentException("Binary operator must be between two literals")
            }
        }
    }
}
