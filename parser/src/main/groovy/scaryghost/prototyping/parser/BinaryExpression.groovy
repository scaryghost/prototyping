package scaryghost.prototyping.parser

class BinaryExpression implements Expression {
    BinaryOperator operator

    BinaryExpression() {
        subExpressions = new Expression[2]
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        BinaryExpression that = (BinaryExpression) o

        if (isGrouped != that.isGrouped) return false
        if (operator != that.operator) return false
        if (subExpressions != that.subExpressions) return false

        return true
    }

    int hashCode() {
        int result
        result = (operator != null ? operator.hashCode() : 0)
        result = 31 * result + (subExpressions != null ? subExpressions.hashCode() : 0)
        result = 31 * result + (isGrouped ? 1 : 0)
        return result
    }
}
