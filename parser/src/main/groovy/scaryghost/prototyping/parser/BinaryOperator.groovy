package scaryghost.prototyping.parser

enum BinaryOperator {
    PLUS("+", 0),
    MINUS("-", 0),
    MULTIPLY("*", 1),
    DIVIDE("/", 1)

    public static Map<String, BinaryOperator> NAMES = BinaryOperator.inject([:]) { acc, e ->
        acc[e.repr] = e
        acc
    }

    public final String repr
    public final int priority

    BinaryOperator(String repr, int priority) {
        this.repr = repr
        this.priority = priority
    }
}