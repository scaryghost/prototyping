package scaryghost.prototyping.grammar

import spock.lang.Specification

class ExpressionGrammarSpecification extends Specification {
    def expression = ExpressionGrammar.instance

    def "simple binary expression"() {
        setup:
        def tokens = ["x", "+", "y"]

        when:
        def possible = expression.iterate(tokens, null)

        then:
        possible.size() == 1
        possible[0] instanceof IdentifierGrammar
    }

    def "negated operand"(List<String> tokens) {
        when:
        def possible = expression.iterate(tokens, null)

        then:
        possible.size() == 1
        possible[0] instanceof IdentifierGrammar

        where:
        tokens                  | _
        ["x", "+", "-", "y"]    | _
        ["-", "x", "+", "y"]    | _
    }


    def "grouped expression"(List<String> tokens) {
        setup:
        def state = new State()

        when:
        def possible = expression.iterate(tokens, state)

        then:
        possible.size() == 1
        possible[0] instanceof IdentifierGrammar
        state.stack.isEmpty()

        where:
        tokens                                  | _
        ["(", "x", "+", "y", ")", "*", "z"]     | _
        ["-", "(", "x", "+", "y", ")"]          | _
    }

    def "extra parenthesis"() {
        setup:
        def state = new State()

        when:
        def possible = expression.iterate(["(", "x", "+", "y", ")", ")", "*", "z"], state)

        then:
        possible.isEmpty()
        state.stack.isEmpty()
    }
}
