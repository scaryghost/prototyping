package scaryghost.prototyping.parser

import spock.lang.Specification

import static scaryghost.prototyping.parser.Util.buildExpression

class UnaryExpressionSpecification extends Specification {
    def "simple unary operator"() {
        when:
        def tree = buildExpression(["-", "x"])

        then:
        tree == new UnaryExpression(operator: UnaryOperator.MINUS).tap {
            it.subExpressions[0] = new Literal(value: "x")
        }
    }

    def "double negation"() {
        when:
        def tree = buildExpression(["-", "-", "x"])

        then:
        tree == new UnaryExpression(operator: UnaryOperator.MINUS).tap {
            it.subExpressions[0] = new UnaryExpression(operator: UnaryOperator.MINUS).tap {
                it.subExpressions[0] = new Literal(value: "x")
            }
        }
    }

    def "binary operation with first unary expression"() {
        when:
        def tree = buildExpression(["-", "x", "*", "y"])

        then:
        tree == new BinaryExpression(operator: BinaryOperator.MULTIPLY).tap {
            it.subExpressions[0] = new UnaryExpression(operator: UnaryOperator.MINUS).tap {
                it.subExpressions[0] = new Literal(value: "x")
            }
            it.subExpressions[1] = new Literal(value: "y")
        }
    }

    def "binary operation with second unary expression"() {
        when:
        def tree = buildExpression(["x", "*", "-", "y"])

        then:
        tree == new BinaryExpression(operator: BinaryOperator.MULTIPLY).tap {
            it.subExpressions[0] = new Literal(value: "x")
            it.subExpressions[1] = new UnaryExpression(operator: UnaryOperator.MINUS).tap {
                it.subExpressions[0] = new Literal(value: "y")
            }
        }
    }
}
