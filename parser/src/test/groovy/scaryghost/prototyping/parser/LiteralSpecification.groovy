package scaryghost.prototyping.parser

import spock.lang.Specification

import static scaryghost.prototyping.parser.Util.buildExpression

class LiteralSpecification extends Specification {
    def "one literal"() {
        when:
        def tree = buildExpression(["x"])

        then:
        tree == new Literal(value: "x")
    }

    def "consecutive literals IllegalArgumentException "(List<String> tokens) {
        when:
        buildExpression(tokens)

        then:
        thrown IllegalArgumentException

        where:
        tokens                  | _
        ["x", "y"]              | _
        ["x", "+", "y", "z"]    | _
    }
}
