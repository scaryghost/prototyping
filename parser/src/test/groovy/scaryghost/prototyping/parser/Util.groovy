package scaryghost.prototyping.parser

class Util {
    static def buildExpression(tokens) {
        return tokens.inject(new ExpressionBuilder()) { builder, token ->
            builder.munch(token)
        }.build()
    }
}
