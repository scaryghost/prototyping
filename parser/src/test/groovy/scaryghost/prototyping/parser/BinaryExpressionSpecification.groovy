package scaryghost.prototyping.parser

import spock.lang.Specification

import static scaryghost.prototyping.parser.BinaryOperator.DIVIDE
import static scaryghost.prototyping.parser.BinaryOperator.MINUS
import static scaryghost.prototyping.parser.BinaryOperator.MULTIPLY
import static scaryghost.prototyping.parser.BinaryOperator.PLUS
import static scaryghost.prototyping.parser.Util.buildExpression

class BinaryExpressionSpecification extends Specification {
    def "equal priority operators"() {
        when:
        def expr = buildExpression(["x", "-", "y", "+", "z"])

        then:
        expr == new BinaryExpression(operator: PLUS).tap {
            subExpressions[0] = new BinaryExpression(operator: MINUS).tap {
                subExpressions[0] = new Literal(value: "x")
                subExpressions[1] = new Literal(value: "y")
            }
            subExpressions[1] = new Literal(value: "z")
        }
    }

    def "higher then equal priority operator"() {
        when:
        def expr = buildExpression(["x", "+", "y", "*", "z", "/", "w"])

        then:
        expr == new BinaryExpression(operator: PLUS).tap {
            subExpressions[0] = new Literal(value: "x")
            subExpressions[1] = new BinaryExpression(operator: DIVIDE).tap {
                subExpressions[0] = new BinaryExpression(operator: MULTIPLY).tap {
                    subExpressions[0] = new Literal(value: "y")
                    subExpressions[1] = new Literal(value: "z")
                }
                subExpressions[1] = new Literal(value: "w")
            }
        }
    }

    def "consecutive binary operators throws IllegalArgumentException"(List<String> tokens) {
        setup:
            def builder = new ExpressionBuilder()

        when:
        tokens.each { builder.munch(it) }

        then:
        thrown IllegalArgumentException

        where:
        tokens                  | _
        ["x", "+", "*"]         | _
        ["x", "+", "/"]         | _
        ["x", "-", "+"]         | _
        ["x", "-", "*"]         | _
        ["x", "-", "/"]         | _
    }

    def "starting with binary op throws IllegalArgumentException"(String op) {
        setup:
        def builder = new ExpressionBuilder()

        when:
        builder.munch(op)

        then:
        thrown IllegalArgumentException

        where:
        op      | _
        "+"     | _
        "*"     | _
        "/"     | _
    }
}
