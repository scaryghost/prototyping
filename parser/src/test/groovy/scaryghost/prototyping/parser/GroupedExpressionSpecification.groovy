package scaryghost.prototyping.parser

import spock.lang.Specification

import static scaryghost.prototyping.parser.BinaryOperator.*
import static scaryghost.prototyping.parser.Util.buildExpression

class GroupedExpressionSpecification extends Specification {
    def "parenthesis overrides precedence"() {
        when:
        def expr = buildExpression(["(", "x", "+", "y", ")", "*", "z", "/", "w"])

        then:
        expr == new BinaryExpression(operator: DIVIDE).tap {
            subExpressions[0] = new BinaryExpression(operator: MULTIPLY).tap {
                subExpressions[0] = new BinaryExpression(operator: PLUS, isGrouped: true).tap {
                    subExpressions[0] = new Literal(value: "x")
                    subExpressions[1] = new Literal(value: "y")
                }
                subExpressions[1] = new Literal(value: "z")
            }
            subExpressions[1] = new Literal(value: "w")
        }
    }

    def "group by parenthesis"() {
        when:
        def expr = buildExpression(["x", "/", "(", "y", "*", "z", ")"])

        then:
        expr == new BinaryExpression(operator: DIVIDE).tap {
            subExpressions[0] = new Literal(value: "x")
            subExpressions[1] = new BinaryExpression(operator: MULTIPLY, isGrouped: true).tap {
                subExpressions[0] = new Literal(value: "y")
                subExpressions[1] = new Literal(value: "z")
            }
        }
    }

    def "negate expression"() {
        when:
        def expr = buildExpression(["-", "(", "y", "+", "z", ")"])

        then:
        expr == new UnaryExpression(operator: UnaryOperator.MINUS).tap {
            subExpressions[0] = new BinaryExpression(operator: PLUS, isGrouped: true).tap {
                subExpressions[0] = new Literal(value: "y")
                subExpressions[1] = new Literal(value: "z")
            }
        }
    }
}
