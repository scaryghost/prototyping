package scaryghost.ktstdlib

import com.github.rschmitt.dynamicobject.DynamicObject
import com.github.rschmitt.dynamicobject.EdnTranslator
import com.github.rschmitt.dynamicobject.FressianWriteHandler
import com.github.rschmitt.dynamicobject.internal.EdnSerialization
import com.github.rschmitt.dynamicobject.internal.FressianSerialization
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.property.PropTestConfig
import io.kotest.property.checkAll
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.fressian.FressianReader
import org.fressian.FressianWriter
import org.fressian.Reader
import org.fressian.Writer
import org.fressian.handlers.ReadHandler
import org.fressian.handlers.WriteHandler
import org.fressian.impl.InheritanceLookup
import org.fressian.impl.MapLookup
import scaryghost.ktstdlib.ktserialization.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.StringReader
import java.nio.file.Paths
import java.time.Instant
import java.util.*
import kotlin.io.path.inputStream


class EdnDecodingTest : FunSpec({
    context("special floats") {
        test("DynamicObject negative zero") {
            val original = DynamicObject.newInstance(EdnPrimitiveReference::class.java)
                .withFloat(-0.0f)
            val edn = DynamicObject.serialize(original)
            val copy = DynamicObject.deserialize(edn, EdnPrimitiveReference::class.java)

            copy.float shouldBe -0.0
        }
        test("fressian negative zero") {
            val os = ByteArrayOutputStream()

            FressianWriter(os).use {
                it.writeDouble(-0.0)
            }

            FressianReader(ByteArrayInputStream(os.toByteArray())).use{
                // Fressian always serializes +/-0.0 as +0.0
                it.readDouble() shouldNotBe -0.0
            }
        }
    }
    context("kotlin decoder understands dynamic object edn") {
        test("primitives") {
            checkAll(arbEdnPrimitiveReference) { original ->
                val edn = DynamicObject.serialize(original)

                val tokenizer = EdnTokenizer(StringReader(edn))
                val decoder = StreamingEdnDecoder(tokenizer.parse())
                val copy = decoder.decodeSerializableValue<EdnPrimitive>(serializer())

                original.int shouldBe copy.int
                original.byte shouldBe copy.byte
                original.short shouldBe copy.short
                original.long shouldBe copy.long

                original.float shouldBe copy.float
                original.double shouldBe copy.double

                original.boolean shouldBe copy.boolean

                original.uuid shouldBe copy.uuid
                original.instant shouldBe copy.instant
            }
        }

        test("tagged values") {
            EdnSerialization.registerTag(EdnTaggedValueReference::class.java, "tagged-type")
            EdnSerialization.registerType(Suit::class.java, EdnSuitTranslator)

            checkAll(arbEdnHandReference()) { hand ->
                val edn = DynamicObject.serialize(hand)
                println(edn)

                val tokenizer = EdnTokenizer(StringReader(edn))
                val decoder = StreamingEdnDecoder(tokenizer.parse())
                val copy = decoder.decodeSerializableValue<List<EdnTaggedValue>>(serializer())

                hand.size shouldBe copy.size
                hand.forEachIndexed { i, card ->
                    card.number shouldBe copy[i].number
                    card.suit shouldBe copy[i].suit
                }
            }

            EdnSerialization.deregisterType(Suit::class.java)
            EdnSerialization.deregisterTag(EdnTaggedValueReference::class.java)
        }

        test("fressian") {
            DynamicObject.registerTag(EdnTaggedValueReference::class.java, "tagged-type")
            DynamicObject.registerType(Suit::class.java, EdnSuitTranslator)
            DynamicObject.registerType(Suit::class.java, "tagged-type", SuitReadHandler) { w, instance ->
                w.writeString(instance.toString())
            }

            checkAll(arbEdnHandReference()) { hand ->
                val fressianBytes = DynamicObject.toFressianByteArray(hand)

                val decoder = FressianDODecoder.from(ByteArrayInputStream(fressianBytes), mapOf(
                    "tagged-type" to SuitReadHandler
                ))
                val copy = decoder.decodeSerializableValue<List<EdnTaggedValue>>(serializer())

                hand.size shouldBe copy.size
                hand.forEachIndexed { i, card ->
                    card.number shouldBe copy[i].number
                    card.suit shouldBe copy[i].suit
                }
            }

            DynamicObject.deregisterType(Suit::class.java)
            DynamicObject.deregisterTag(EdnTaggedValueReference::class.java)
        }

        test("another tagged") {
            val zacian = Pokemon(
                hp = 92,
                attack = 120,
                defense = 115,
                specialAttack = 80,
                specialDefense = 115,
                speed = 138
            )

            DynamicObject.registerType(Pokemon::class.java, PokemonTranslator)

            val edn = DynamicObject.serialize(zacian).also { println(it) }

            val tokenizer = EdnTokenizer(StringReader(edn))
            val decoder = StreamingEdnDecoder(tokenizer.parse())

            val copy = decoder.decodeSerializableValue(PokemonSerializer)

            zacian shouldBe copy

            DynamicObject.deregisterType(Pokemon::class.java)
        }
    }

    context("dynamic object understands kotlin encoded edn") {
        test("primitives") {
            checkAll(arbEdnPrimitive) {
                val baos = ByteArrayOutputStream()
                val encoder = EdnEncoder(baos)
                encoder.encodeSerializableValue(serializer(), it)
                encoder.writer.flush()
                encoder.writer.close()

                val copy = DynamicObject.deserialize(baos.toString(), EdnPrimitiveReference::class.java)

                it.int shouldBe copy.int
                it.byte shouldBe copy.byte
                it.short shouldBe copy.short
                it.long shouldBe copy.long

                it.float shouldBe copy.float
                it.double shouldBe copy.double

                it.boolean shouldBe copy.boolean

                it.uuid shouldBe copy.uuid
                it.instant shouldBe copy.instant
            }
        }

        test("fressian") {
            FressianDOEncoder.tags[EdnPrimitive::class.java.canonicalName] = "primitives"
            DynamicObject.registerTag(EdnPrimitiveReference::class.java, "primitives")

            checkAll(arbEdnPrimitive) {
                val encoder = FressianDOEncoder()
                encoder.encodeSerializableValue(serializer(), it)

                val os = ByteArrayOutputStream()
                val writer = FressianWriter(os, InheritanceLookup(MapLookup(
                    mapOf(
                        EdnKeyword::class.java to mapOf("key" to EdnWriteKeyHandler),
                        Map::class.java to mapOf("primitives" to MapWriteHandler(EdnPrimitive::class.java, "primitives"))
                    )))
                )
                writer.writeObject(encoder.entriesAsMap)

                val copy = DynamicObject.fromFressianByteArray<EdnPrimitiveReference>(os.toByteArray())

                it.int shouldBe copy.int
                it.byte shouldBe copy.byte
                it.short shouldBe copy.short
                it.long shouldBe copy.long

                it.float shouldBe copy.float
                it.double shouldBe copy.double

                it.boolean shouldBe copy.boolean

                it.uuid shouldBe copy.uuid
                it.instant shouldBe copy.instant
            }
        }

        test("tagged values") {
            EdnSerialization.registerTag(EdnTaggedValueReference::class.java, "tagged-type")
            EdnSerialization.registerType(Suit::class.java, object: EdnTranslator<Suit> {
                override fun read(obj: Any?): Suit? {
                    return obj?.let { Suit.valueOf(it.toString()) }
                }

                override fun write(obj: Suit?): String {
                    return obj?.name?.let { "\"$it\"" } ?: "nil"
                }

                override fun getTag(): String = Suit::class.qualifiedName!!
            })
            EdnEncoder.tags[EdnTaggedValue::class.qualifiedName!!] = "tagged-type"

            checkAll(arbEdnHand()) { hand ->
                val baos = ByteArrayOutputStream()
                val encoder = EdnEncoder(baos)
                encoder.encodeSerializableValue(serializer(), hand)
                encoder.writer.flush()
                encoder.writer.close()

                val copy = DynamicObject.deserialize(baos.toString(), List::class.java)

                hand.size shouldBe copy.size
                hand.forEachIndexed { i, card ->
                    val e = copy[i] as EdnTaggedValueReference

                    card.number shouldBe e.number
                    card.suit shouldBe e.suit
                }
            }

            EdnEncoder.tags.remove(EdnTaggedValue::class.qualifiedName!!)
            EdnSerialization.deregisterType(Suit::class.java)
            EdnSerialization.deregisterTag(EdnTaggedValueReference::class.java)
        }

        test("another tagged") {
            val quagsire = Pokemon(
                hp = 95,
                attack = 85,
                defense = 85,
                specialAttack = 65,
                specialDefense = 65,
                speed = 35
            )

            EdnEncoder.registerTranslator(PokemonSerializer)

            val baos = ByteArrayOutputStream()
            val encoder = EdnEncoder(baos)
            encoder.encodeSerializableValue(PokemonSerializer, quagsire)
            encoder.writer.flush()
            encoder.writer.close()

            DynamicObject.registerType(Pokemon::class.java, PokemonTranslator)

            val copy = DynamicObject.deserialize(baos.toString(), Pokemon::class.java)

            quagsire shouldBe copy

            DynamicObject.deregisterType(Pokemon::class.java)
        }
    }
})

@Serializable
data class EdnPrimitive(
    @SerialName("integer") val int: Int?,
    val byte: Byte?,
    @Contextual val uuid: UUID?,
    val long: Long?,
    val boolean: Boolean?,
    val float: Float?,
    @Contextual val instant: Instant?,
    val double: Double?,
    val string: String?,
    val short: Short?,
)

@Serializable()
data class EdnTaggedValue(
    val suit: Suit,
    val number: Int,
)

data class Pokemon(
    val hp: Int,
    val attack: Int,
    val defense: Int,
    val specialAttack: Int,
    val specialDefense: Int,
    val speed: Int
)

object PokemonSerializer : KSerializer<Pokemon> {
    override val descriptor: SerialDescriptor
    get() = PrimitiveSerialDescriptor(Pokemon::class.qualifiedName!!, PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Pokemon {
        val parts = decoder.decodeString().split(" ")
        return Pokemon(
            hp = parts[0].toInt(),
            attack = parts[1].toInt(),
            defense = parts[2].toInt(),
            specialAttack = parts[3].toInt(),
            specialDefense = parts[4].toInt(),
            speed = parts[5].toInt(),
        )
    }

    override fun serialize(encoder: Encoder, value: Pokemon) {
        encoder.encodeString("${value.hp} ${value.attack} ${value.defense} ${value.specialAttack} ${value.specialDefense} ${value.speed}")
    }
}

object PokemonTranslator : EdnTranslator<Pokemon> {
    override fun read(obj: Any?): Pokemon? {
        return obj?.let {
            val parts = it.toString().split(" ")
            Pokemon(
                hp = parts[0].toInt(),
                attack = parts[1].toInt(),
                defense = parts[2].toInt(),
                specialAttack = parts[3].toInt(),
                specialDefense = parts[4].toInt(),
                speed = parts[5].toInt(),
            )
        }
    }

    override fun write(obj: Pokemon?): String {
        return obj
            ?.let { "\"${it.hp} ${it.attack} ${it.defense} ${it.specialAttack} ${it.specialDefense} ${it.speed}\"" }
            ?: "nil"
    }

    override fun getTag(): String =
        Pokemon::class.java.canonicalName
}


object EdnSuitTranslator : EdnTranslator<Suit> {
    override fun read(obj: Any?): Suit? {
        return obj?.let { Suit.valueOf(it.toString()) }
    }

    override fun write(obj: Suit?): String {
        return obj?.name?.let { "\"$it\"" } ?: "nil"
    }

    override fun getTag(): String = Suit::class.qualifiedName!!
}

object SuitReadHandler: ReadHandler {
    override fun read(r: Reader, tag: Any, componentCount: Int): Any {
        val value = r.readObject() as Map<*, *>
        return value
    }
}