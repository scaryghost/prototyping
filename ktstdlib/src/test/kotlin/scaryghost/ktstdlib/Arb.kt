package scaryghost.ktstdlib

import com.github.rschmitt.dynamicobject.DynamicObject
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import java.time.Instant
import java.time.temporal.ChronoUnit

val arbEdnPrimitiveReference = arbitrary {
    var reference = DynamicObject.newInstance(EdnPrimitiveReference::class.java)

    Arb.choose(9 to Arb.int(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withInt(it) }

    Arb.choose(9 to Arb.short(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withShort(it) }

    Arb.choose(9 to Arb.long(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withLong(it) }

    Arb.choose(9 to Arb.byte(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withByte(it) }

    Arb.choose(9 to arbNumericalFloat(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withFloat(it) }

    Arb.choose(9 to arbNumericalDouble(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withDouble(it) }

    Arb.choose(9 to Arb.boolean(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withBoolean(it) }

    Arb.choose(9 to Arb.string(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withString(it) }

    Arb.choose(9 to arbInstant(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withInstant(it) }

    Arb.choose(9 to Arb.uuid(),1 to Arb.constant(null)).bind()
        ?.also { reference = reference.withUuid(it) }

    reference
}

val arbEdnPrimitive = arbitrary {
    EdnPrimitive(
        int = Arb.choose(9 to Arb.int(),1 to Arb.constant(null)).bind(),
        short = Arb.choose(9 to Arb.short(),1 to Arb.constant(null)).bind(),
        long = Arb.choose(9 to Arb.long(),1 to Arb.constant(null)).bind(),
        byte = Arb.choose(9 to Arb.byte(),1 to Arb.constant(null)).bind(),
        float = Arb.choose(9 to arbNumericalFloat(),1 to Arb.constant(null)).bind(),
        double = Arb.choose(9 to arbNumericalDouble(),1 to Arb.constant(null)).bind(),
        boolean = Arb.choose(9 to Arb.boolean(),1 to Arb.constant(null)).bind(),
        string = Arb.choose(9 to Arb.string(),1 to Arb.constant(null)).bind(),
        instant = Arb.choose(9 to arbInstant(),1 to Arb.constant(null)).bind(),
        uuid = Arb.choose(9 to Arb.uuid(),1 to Arb.constant(null)).bind()
    )
}

/** NaN and Infinity not supported in EDN deserialization */
fun arbNumericalFloat() =
    Arb.float()
        .filter { !it.isNaN() && !it.isInfinite() }

fun arbNumericalDouble() =
    Arb.double()
        .filter { !it.isNaN() && !it.isInfinite() }

fun arbInstant() =
    Arb.instant(Instant.EPOCH .. Instant.now())
        .map { it.truncatedTo(ChronoUnit.MILLIS) }
fun arbEdnHandReference(): Arb<List<EdnTaggedValueReference>> {
    val allCards = (1 .. 13).flatMap { value ->
        (0 .. 3).map { suit ->
            value to suit
        }
    }

    return Arb.set(Arb.int(allCards.indices), 5 .. 5)
        .map { indices ->
            indices.map {
                val (value, suit) = allCards[it]
                DynamicObject.newInstance(EdnTaggedValueReference::class.java)
                    .withNumber(value)
                    .withSuit(Suit.entries[suit])
            }
        }
}

fun arbEdnHand(): Arb<Collection<EdnTaggedValue>> {
    val allCards = (1 .. 13).flatMap { value ->
        (0 .. 3).map { suit ->
            value to suit
        }
    }

    fun Set<Int>.toEdnTaggedValues() =
        this.asSequence()
            .map {
                val (value, suit) = allCards[it]
                EdnTaggedValue(Suit.entries[suit], value)
            }

    return Arb.choose(
        1 to Arb.set(Arb.int(allCards.indices), 5 .. 5)
            .map { it.toEdnTaggedValues().toList() },
        1 to Arb.set(Arb.int(allCards.indices), 5 .. 5)
            .map { it.toEdnTaggedValues().toSet() }
    )
}