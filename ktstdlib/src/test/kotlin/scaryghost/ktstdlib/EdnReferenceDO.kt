package scaryghost.ktstdlib

import com.github.rschmitt.dynamicobject.DynamicObject
import com.github.rschmitt.dynamicobject.Key
import scaryghost.ktstdlib.ktserialization.EdnNode
import java.time.Instant
import java.util.UUID

interface EdnPrimitiveReference : DynamicObject<EdnPrimitiveReference> {
    @get:Key(":integer") val int: Int?
    @Key(":integer") fun withInt(value: Int): EdnPrimitiveReference

    @get:Key(":byte") val byte: Byte?
    @Key(":byte") fun withByte(value: Byte): EdnPrimitiveReference

    @get:Key(":short") val short: Short?
    @Key(":short") fun withShort(value: Short): EdnPrimitiveReference

    @get:Key(":long") val long: Long?
    @Key(":long") fun withLong(value: Long): EdnPrimitiveReference

    @get:Key(":string") val string: String?
    @Key(":string") fun withString(value: String): EdnPrimitiveReference

    @get:Key(":double") val double: Double?
    @Key(":double") fun withDouble(value: Double): EdnPrimitiveReference

    @get:Key(":float") val float: Float?
    @Key(":float") fun withFloat(value: Float): EdnPrimitiveReference

    @get:Key(":boolean") val boolean: Boolean?
    @Key(":boolean") fun withBoolean(value: Boolean): EdnPrimitiveReference

    @get:Key(":instant") val instant: Instant?
    @Key(":instant") fun withInstant(value: Instant): EdnPrimitiveReference

    @get:Key(":uuid") val uuid: UUID?
    @Key(":uuid") fun withUuid(value: UUID): EdnPrimitiveReference
}
interface EdnTaggedValueReference : DynamicObject<EdnTaggedValueReference> {
    @get:Key(":number") val number: Int
    @Key(":number") fun withNumber(value: Int): EdnTaggedValueReference

    @get:Key(":suit") val suit: Suit
    @Key(":suit") fun withSuit(value: Suit): EdnTaggedValueReference
}

enum class Suit {
    SPADES,
    HEARTS,
    CLUBS,
    DIAMONDS
}
