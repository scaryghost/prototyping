package org.fressian

import org.fressian.handlers.ILookup
import org.fressian.handlers.ReadHandler
import org.fressian.impl.Codes.*
import java.io.InputStream
import java.lang.reflect.Method

class StreamingFressianReader(
    `is`: InputStream,
    handlerLookup: ILookup<Any, ReadHandler>? = null,
    validateAdler: Boolean = true,
) : FressianReader(`is`, handlerLookup, validateAdler) {
    private val privateReadRawByte: Method
    private val privateRead: Method

    init {
        val clazz = FressianReader::class.java

        privateReadRawByte = clazz.getDeclaredMethod("readNextCode").apply {
            trySetAccessible()
        }
        privateRead = clazz.getDeclaredMethod("read", Int::class.java).apply {
            trySetAccessible()
        }
    }

    override fun readObject(): Any =
        when(val code = privateReadRawByte.invoke(this) as Int) {
            MAP -> readObject()
            LIST_PACKED_LENGTH_START, LIST_PACKED_LENGTH_START + 1,
            LIST_PACKED_LENGTH_START + 2, LIST_PACKED_LENGTH_START + 3,
            LIST_PACKED_LENGTH_START + 4, LIST_PACKED_LENGTH_START + 5,
            LIST_PACKED_LENGTH_START + 6, LIST_PACKED_LENGTH_START + 7 -> FressianIterator(code - LIST_PACKED_LENGTH_START)
            LIST -> FressianIterator(readInt().toInt())
            else -> privateRead.invoke(this, code)
        }

    inner class FressianIterator(private val numElements: Int) : Iterator<Any> {
        private var count = 0

        override fun hasNext(): Boolean {
            return count < numElements
        }

        override fun next(): Any {
            if (count >= numElements) {
                throw NoSuchElementException("No more elements available, exceeded $numElements")
            }

            return readObject().also {
                count++
            }
        }

    }
}