package scaryghost.ktstdlib.ktserialization

fun EdnTokenizer.parse(): StreamingEdnNode {
    return when(val token = this.next()) {
        is EdnSymbolToken -> {
            when(token.value) {
                "{" -> StreamingEdnMapNode(this)
                "#{" -> StreamingEdnSetNode(this)
                "[" -> StreamingEdnListNode(this)
                else -> throw ParserException("Unrecognized EDN symbol: $token", token)
            }
        }
        is EdnKeywordToken -> EvaledEdnKeywordNode(token.value)
        is EdnLongToken -> EvaledEdnLongNode(token.value.toLong())
        is EdnBooleanToken -> EvaledEdnBooleanNode(token.value.toBoolean())
        is EdnDoubleToken -> EvaledEdnDoubleNode(token.value.toDouble())
        is EdnNilToken -> EvaledEdnNilNode
        is EdnStringToken -> EvaledEdnStringNode(token.value)
        is EdnTagToken -> StreamingEdnTagNode(token.value, this)
    }
}

sealed interface StreamingEdnNode {
    fun flatten(): EvaledEdnNode
}

interface EvaledEdnNode : StreamingEdnNode {
    override fun flatten(): EvaledEdnNode = this
}
@JvmInline
value class EvaledEdnLongNode(val value: Long): EvaledEdnNode

@JvmInline
value class EvaledEdnBooleanNode(val value: Boolean): EvaledEdnNode

@JvmInline
value class EvaledEdnDoubleNode(val value: Double): EvaledEdnNode

@JvmInline
value class EvaledEdnKeywordNode(val value: String): EvaledEdnNode

data object EvaledEdnNilNode: EvaledEdnNode

@JvmInline
value class EvaledEdnStringNode(val value: String): EvaledEdnNode

@JvmInline
value class EvaledEdnSetNode(val value: Set<EvaledEdnNode>) : EvaledEdnNode

@JvmInline
value class EvaledEdnListNode(val value: List<EvaledEdnNode>) : EvaledEdnNode

@JvmInline
value class EvaledEdnMapNode(val value: List<EvaledEdnNode>) : EvaledEdnNode

data class EvaledEdnTagNode(val tag: String, val child: EvaledEdnNode): EvaledEdnNode

class StreamingEdnTagNode(val tag: String, private val tokenizer: EdnTokenizer): StreamingEdnNode {
    override fun flatten(): EvaledEdnNode = EvaledEdnTagNode(tag, tokenizer.parse().flatten())
    fun child(): StreamingEdnNode = tokenizer.parse()
}

class StreamingEdnListNode(private val tokenizer: EdnTokenizer): StreamingEdnNode {
    override fun flatten(): EvaledEdnNode {
        return EvaledEdnListNode(
            buildList {
                while(true) {
                   try {
                       add(next().flatten())
                   } catch (e: NoSuchElementException) {
                       break
                   }
                }
            }
        )
    }

    fun next(): StreamingEdnNode {
        return try {
            tokenizer.parse()
        } catch (e: ParserException) {
            when(e.token) {
                is EdnSymbolToken -> {
                    if (e.token.value == "]") {
                        throw NoSuchElementException("No more elements")
                    } else {
                        throw ParserException("Unrecognized list symbol: '${e.token}'", e.token, e)
                    }
                }
                else -> tokenizer.parse()
            }
        }
    }
}

class StreamingEdnSetNode(private val tokenizer: EdnTokenizer): StreamingEdnNode {
    override fun flatten(): EvaledEdnNode {
        return EvaledEdnSetNode(
            buildSet {
                while(true) {
                    try {
                        add(next().flatten())
                    } catch (e: NoSuchElementException) {
                        break
                    }
                }
            }
        )
    }

    fun next(): StreamingEdnNode {
        return try {
            tokenizer.parse()
        } catch (e: ParserException) {
            when(e.token) {
                is EdnSymbolToken -> {
                    if (e.token.value == "}") {
                        throw NoSuchElementException("No more list elements")
                    } else {
                        throw ParserException("Unrecognized list symbol: '${e.token}'", e.token, e)
                    }
                }
                else -> tokenizer.parse()
            }
        }
    }
}

class StreamingEdnMapNode(private val tokenizer: EdnTokenizer): StreamingEdnNode {
    private var nextChild: () -> StreamingEdnNode = ::childKey

    fun next(): StreamingEdnNode = nextChild.invoke()

    private fun childKey(): StreamingEdnNode =
        try {
            tokenizer.parse()
                .also { nextChild = ::childValue }
        } catch (e: ParserException) {
            when(e.token) {
                is EdnSymbolToken -> {
                    if (e.token.value == "}") {
                        throw NoSuchElementException("No more map elements")
                    } else {
                        throw ParserException("Unrecognized map symbol: '${e.token}'", e.token, e)
                    }
                }
                else -> tokenizer.parse()
            }
        }

    private fun childValue(): StreamingEdnNode = tokenizer.parse()
        .also { nextChild = ::childKey }

    override fun flatten(): EvaledEdnNode {
        return EvaledEdnMapNode(
            buildList {
                while(true) {
                    val nextKey = try {
                        next()
                    } catch (e: NoSuchElementException) {
                        break
                    }.flatten()
                    val nextValue = next().flatten()

                    add(nextKey)
                    add(nextValue)
                }
            }
        )
    }
}
