package scaryghost.ktstdlib.ktserialization

class EdnSlurper(val tokenizer: EdnTokenizer) {

}

class ParserException(msg: String, val token: EdnToken, cause: Throwable? = null) : RuntimeException(msg, cause)

fun EdnTokenizer.decodeRoot(): EdnNode {
    return when(val token = this.next()) {
        is EdnSymbolToken -> {
            when(token.value) {
                "{" -> this.decodeMap()
                "#{" -> this.decodeSet()
                "[" -> this.decodeList()
                else -> throw ParserException("Unrecognized EDN symbol: $token", token)
            }
        }
        is EdnLongToken -> EdnLongNode(token.value.toLong())
        is EdnKeywordToken -> throw IllegalArgumentException("EDN string cannot start with a keyword: $token")
        is EdnTagToken -> EdnTagNode(token.value, this.decodeRoot())
        is EdnStringToken -> EdnStringNode(token.value)
        is EdnDoubleToken -> EdnDoubleNode(token.asDouble())
        is EdnBooleanToken -> EdnBooleanNode(token.value.toBoolean())
        is EdnNilToken -> EdnNilNode
    }
}

fun EdnTokenizer.decodeMap(): EdnNode {
    val elements = mutableMapOf<String, EdnNode>()

    while(true) {
        val key = when(val token = this.next()) {
            is EdnSymbolToken -> {
                if (token.value == "}") {
                    break
                } else {
                    throw IllegalArgumentException("Unrecognized map symbol: '$token'")
                }
            }
            is EdnKeywordToken -> token.value
            else -> throw IllegalArgumentException("EDN map must begin with a key, received: $token")
        }

        elements[key] = this.decodeRoot()
    }

    return EdnMapNode(elements)
}

fun EdnTokenizer.decodeSet(): EdnNode {
    val elements = mutableSetOf<EdnNode>()

    while(true) {
        elements.add(
            try {
                this.decodeRoot()
            } catch (e: ParserException) {
                when(e.token) {
                    is EdnSymbolToken -> {
                        if (e.token.value == "}") {
                            break
                        } else {
                            throw ParserException("Unrecognized set symbol: '$e.token'", e.token, e)
                        }
                    }
                    else -> this.decodeRoot()
                }
            }
        )
    }

    return EdnSetNode(elements.toList())
}

fun EdnTokenizer.decodeList(): EdnNode {
    val elements = mutableListOf<EdnNode>()

    while(true) {
        elements.add(
            try {
                this.decodeRoot()
            } catch (e: ParserException) {
                when(e.token) {
                    is EdnSymbolToken -> {
                        if (e.token.value == "]") {
                            break
                        } else {
                            throw ParserException("Unrecognized list symbol: '${e.token}'", e.token, e)
                        }
                    }
                    else -> this.decodeRoot()
                }
            }
        )
    }

    return EdnListNode(elements)
}

sealed interface EdnNode

@JvmInline
value class EdnLongNode(val value: Long) : EdnNode

@JvmInline
value class EdnDoubleNode(val value: Double) : EdnNode

@JvmInline
value class EdnMapNode(val value: Map<String, EdnNode>) : EdnNode

@JvmInline
value class EdnSetNode(val value: List<EdnNode>) : EdnNode

@JvmInline
value class EdnListNode(val value: List<EdnNode>) : EdnNode

@JvmInline
value class EdnStringNode(val value: String) : EdnNode

@JvmInline
value class EdnBooleanNode(val value: Boolean) : EdnNode

data object EdnNilNode : EdnNode

data class EdnTagNode(val tag: String, val node: EdnNode) : EdnNode