package scaryghost.ktstdlib.ktserialization

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.CompositeDecoder.Companion.DECODE_DONE
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.plus
import kotlinx.serialization.modules.serializersModuleOf

class StreamingEdnDecoder(
    private val node: StreamingEdnNode
) : Decoder {
    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            when(node) {
                is StreamingEdnMapNode -> StreamingStructDecoder(node)
                is EvaledEdnMapNode -> EvaledStructDecoder(node)
                is StreamingEdnTagNode -> {
                    val child = node.child()

                    check(child is StreamingEdnMapNode)
                    StreamingStructDecoder(child)
                }
                is EvaledEdnTagNode -> {
                    check(node.child is EvaledEdnMapNode)
                    EvaledStructDecoder(node.child)
                }
                else -> throw IllegalArgumentException("Invalid node type received for decoding a class: ${node::class}")
            }
        }
        StructureKind.LIST -> {
            when(node) {
                is StreamingEdnListNode -> StreamingListDecoder(node::next, node::flatten)
                is EvaledEdnListNode -> StreamingListDecoder(node.value.iterator()::next, node::flatten)
                is StreamingEdnSetNode -> StreamingListDecoder(node::next, node::flatten)
                is EvaledEdnSetNode -> StreamingListDecoder(node.value.iterator()::next, node::flatten)
                else -> throw IllegalArgumentException("Invalid node type received for decoding a collection: ${node::class}")
            }
        }
        StructureKind.MAP -> {
            when(node) {
                is StreamingEdnMapNode -> StreamingListDecoder(node::next, node::flatten)
                is EvaledEdnMapNode -> StreamingListDecoder(node.value.iterator()::next, node::flatten)
                else -> throw IllegalArgumentException("Invalid node type received for decoding a map: ${node::class}")
            }
        }
        else -> throw IllegalArgumentException("${descriptor.kind} not supported")
    }

    override fun decodeBoolean(): Boolean {
        check(node is EvaledEdnBooleanNode)
        return node.value
    }

    override fun decodeByte(): Byte {
        check(node is EvaledEdnLongNode)
        return node.value.toByte()
    }

    override fun decodeChar(): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDouble(): Double {
        check(node is EvaledEdnDoubleNode)
        return node.value
    }

    override fun decodeEnum(enumDescriptor: SerialDescriptor): Int {
        return when(node) {
            is StreamingEdnTagNode -> {
                val child = node.child()
                check(child is EvaledEdnStringNode)

                enumDescriptor.getElementIndex(child.value).also {
                    check(it != CompositeDecoder.UNKNOWN_NAME) {
                        "Unexpected enum value '${child.value}' for ${node.tag}"
                    }
                }
            }
            is EvaledEdnTagNode -> {
                check(node.child is EvaledEdnStringNode)
                enumDescriptor.getElementIndex(node.child.value).also {
                    check(it != CompositeDecoder.UNKNOWN_NAME) {
                        "Unexpected enum value '${node.child.value}' for ${node.tag}"
                    }
                }
            }
            else -> throw IllegalArgumentException("Unsure how to decode as enum ${node::class}")
        }
    }

    override fun decodeFloat(): Float {
        check(node is EvaledEdnDoubleNode)

        return node.value.toFloat()
    }

    override fun decodeInline(descriptor: SerialDescriptor): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeInt(): Int {
        check(node is EvaledEdnLongNode)
        return node.value.toInt()
    }

    override fun decodeLong(): Long {
        check(node is EvaledEdnLongNode)
        return node.value.toLong()
    }

    @ExperimentalSerializationApi
    override fun decodeNotNullMark(): Boolean {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun decodeNull(): Nothing? {
        check(node is EvaledEdnNilNode)
        return null
    }

    override fun decodeShort(): Short {
        check(node is EvaledEdnLongNode)
        return node.value.toShort()
    }

    override fun decodeString(): String {
        return when(node) {
            is EvaledEdnStringNode -> node.value
            is EvaledEdnTagNode -> {
                check(node.child is EvaledEdnStringNode)

                node.child.value
            }
            is StreamingEdnTagNode -> {
                val flattened = node.flatten()
                check(flattened is EvaledEdnTagNode)
                check(flattened.child is EvaledEdnStringNode)

                flattened.child.value
            }
            else -> throw IllegalArgumentException("Unable to extract a string value from ${node::class}")
        }
    }
}

class StreamingListDecoder(
    private val nodeIterator: () -> StreamingEdnNode,
    private val flatten: () -> StreamingEdnNode
) : CompositeDecoder {
    private var isExhausted = false
    private var next: StreamingEdnNode? = null
    private var count = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        val nextElement = next
        check(nextElement is EvaledEdnBooleanNode)
        return nextElement.value
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        val nextElement = next
        check(nextElement is EvaledEdnLongNode)
        return nextElement.value.toByte()
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        val nextElement = next
        check(nextElement is EvaledEdnDoubleNode)
        return nextElement.value
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        pullNextElement()
        return when(isExhausted) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        val nextElement = next
        check(nextElement is EvaledEdnDoubleNode)
        return nextElement.value.toFloat()
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val nextElement = next
        check(nextElement is EvaledEdnLongNode)
        return nextElement.value.toInt()
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        val nextElement = next
        check(nextElement is EvaledEdnLongNode)
        return nextElement.value
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T?>,
        previousValue: T?
    ): T? {
        return when(val nextElement = next) {
            null, is EvaledEdnNilNode -> null
            else -> deserializer.deserialize(StreamingEdnDecoder(nextElement))
        }
    }

    override fun <T> decodeSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T>,
        previousValue: T?
    ): T {
        val nextElement = next
        check(nextElement is StreamingEdnNode)
        return deserializer.deserialize(StreamingEdnDecoder(nextElement))
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        val nextElement = next
        check(nextElement is EvaledEdnLongNode)
        return nextElement.value.toShort()
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        val nextElement = next
        check(nextElement is EvaledEdnStringNode)
        return nextElement.value
    }

    override fun endStructure(descriptor: SerialDescriptor) {
        flatten()
    }

    private fun pullNextElement() {
        next = try {
            nodeIterator()
        } catch (e: NoSuchElementException) {
            isExhausted = true
            null
        }
    }
}

class StreamingStructDecoder(
    private val node: StreamingEdnMapNode
) : CompositeDecoder {
    private val cache = mutableMapOf<String, StreamingEdnNode>()

    private var isExhausted = false
    private var count = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnBooleanNode)
        return node.value
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnLongNode)
        return node.value.toByte()
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnDoubleNode)
        return node.value
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= descriptor.elementsCount) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnDoubleNode)
        return node.value.toFloat()
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnLongNode)
        return node.value.toInt()
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnLongNode)
        return node.value
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        return when(val node = findNode(descriptor, index)) {
            null -> null
            is EvaledEdnNilNode -> null
            else -> deserializer.deserialize(StreamingEdnDecoder(node))
        }
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        return when(val node = findNode(descriptor, index)) {
            null, is EvaledEdnNilNode -> throw IllegalArgumentException("non-null field ${descriptor.getElementName(index)} was not found in decoded map")
            else -> deserializer.deserialize(StreamingEdnDecoder(node))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnLongNode)
        return node.value.toShort()
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        val node = findNode(descriptor, index)

        check(node is EvaledEdnStringNode)
        return node.value
    }

    override fun endStructure(descriptor: SerialDescriptor) {
        node.flatten()
    }

    private fun findNode(descriptor: SerialDescriptor, index: Int): StreamingEdnNode? {
        val key = ":${descriptor.getElementName(index)}"
        if (key !in cache) {
            if (!isExhausted) {
                while (true) {
                    val nextKey = try {
                        node.next()
                    } catch (e: NoSuchElementException) {
                        isExhausted = true
                        return null
                    }
                    val strKey = when (nextKey) {
                        is EvaledEdnStringNode -> nextKey.value
                        is EvaledEdnKeywordNode -> nextKey.value
                        else -> throw IllegalArgumentException("key must be string or keyword when decoding a struct, received: ${nextKey::class}")
                    }
                    val nextValue = node.next().flatten()

                    if (strKey == key) {
                        return nextValue
                    }

                    cache[strKey] = nextValue
                }
            } else {
                return null
            }
        } else {
            return cache.remove(key)
        }
    }
}

class EvaledStructDecoder(
    private val node: EvaledEdnMapNode
) : CompositeDecoder {
    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)
    private var count = 0

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        val element = node.value[index]
        check(element is EvaledEdnBooleanNode)

        return element.value
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        val element = node.value[index]
        check(element is EvaledEdnLongNode)

        return element.value.toByte()
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        val element = node.value[index]
        check(element is EvaledEdnDoubleNode)

        return element.value
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= node.value.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        val element = node.value[index]
        check(element is EvaledEdnDoubleNode)

        return element.value.toFloat()
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val element = node.value[index]
        check(element is EvaledEdnLongNode)

        return element.value.toInt()
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T?>,
        previousValue: T?
    ): T? {
        TODO("Not yet implemented")
    }

    override fun <T> decodeSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T>,
        previousValue: T?
    ): T {
        TODO("Not yet implemented")
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        val element = node.value[index]
        check(element is EvaledEdnLongNode)

        return element.value.toShort()
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        val element = node.value[index]
        check(element is EvaledEdnStringNode)

        return element.value
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }

}