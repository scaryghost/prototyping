package scaryghost.ktstdlib.ktserialization

import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.Instant

@Serializable
data class MobileSuit(
    val weight: Float,
    val pilots: Set<Pilot>,
    val height: Float,
    @SerialName("meteor-compatible") val meteorCompatible: Boolean,
    val armaments: List<Armament>,
    val name: String,
    @Contextual val debut: Instant,
)

@Serializable
data class Pilot(
    val firstName: String,
    val lastName: String
)

@Serializable
data class Armament(
    val name: String,
    val count: Int,
    val placement: Placement
) {
    enum class Placement {
        FIXED,
        HANDHELD
    }
}



/*-
interface Armament : DynamicObject<Armament> {
    @Key(":name") fun getName(): String
    @Key(":name") fun withName(name: String): Armament

    @Key(":count") fun getCount(): Int
    @Key(":count") fun withCount(count: Int): Armament

    @Key(":placement") fun getPlacement(): ArmamentPlacement
    @Key(":placement") fun withPlacement(placement: ArmamentPlacement): Armament
}
 */