package scaryghost.ktstdlib.ktserialization

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.CompositeDecoder.Companion.DECODE_DONE
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.modules.SerializersModule
import org.fressian.FressianReader
import org.fressian.Reader
import org.fressian.handlers.ReadHandler
import org.fressian.impl.MapLookup
import scaryghost.ktstdlib.EdnReadByteHandler
import scaryghost.ktstdlib.EdnReadCharHandler
import scaryghost.ktstdlib.EdnReadKeyHandler
import scaryghost.ktstdlib.EdnReadMapHandler
import java.io.InputStream
import kotlin.io.path.inputStream

// https://github.com/clojure/data.fressian/blob/e427eece8903dd471e829fb74da52a0fe7e20e70/src/main/clojure/clojure/data/fressian.clj#L152
class FressianReadHandler<T>(
    private val type: Class<T>
) : ReadHandler {
    override fun read(reader: Reader, tag: Any, componentCount: Int): Any {
        TODO()
    }

}

class FressianDODecoder(
    private val obj: Any
) : Decoder {
    companion object {
        fun from(ins: InputStream, readHandlers: Map<String, ReadHandler>) = FressianDODecoder(
            ins.use {
                FressianReader(it, MapLookup(
                    mapOf(
                        "map" to EdnReadMapHandler,
                        "key" to EdnReadKeyHandler,
                        "byte" to EdnReadByteHandler,
                        "char" to EdnReadCharHandler,
                    ) + readHandlers
                )).readObject()
            }.also(::println)
        )
    }

    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            when {
                obj is Map<*, *> -> FressianMapDecoder(obj)
                else -> throw IllegalArgumentException("Expecting a map , received: ${obj::class}")
            }
        }
        StructureKind.LIST -> {
            when(obj) {
                is List<*> -> FressianListDecoder(obj)
                is Set<*> -> FressianListDecoder(obj.toList())
                else -> throw IllegalArgumentException("Expecting list or set , received: ${obj::class}")
            }
        }
        else -> throw IllegalArgumentException("${descriptor.kind} not supported")
    }

    override fun decodeBoolean(): Boolean {
        return obj as Boolean
    }

    override fun decodeByte(): Byte {
        return obj as Byte
    }

    override fun decodeChar(): Char {
        return obj as Char
    }

    override fun decodeDouble(): Double {
        return obj as Double
    }

    override fun decodeEnum(enumDescriptor: SerialDescriptor): Int {
        return when(obj) {
            is String -> {
                enumDescriptor.getElementIndex(obj).also {
                    check(it != CompositeDecoder.UNKNOWN_NAME) {
                        "Unexpected enum name '$obj' for ${enumDescriptor.serialName}"
                    }
                }
            }
            else -> throw IllegalArgumentException("Expected obj to be a string, it was ${obj::class}")
        }
    }

    override fun decodeFloat(): Float {
        return obj as Float
    }

    override fun decodeInline(descriptor: SerialDescriptor): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeInt(): Int {
        return obj as Int
    }

    override fun decodeLong(): Long {
        return obj as Long
    }

    @ExperimentalSerializationApi
    override fun decodeNotNullMark(): Boolean {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun decodeNull(): Nothing? {
        TODO("Not yet implemented")
    }

    override fun decodeShort(): Short {
        return obj as Short
    }

    override fun decodeString(): String {
        return obj as String
    }
}

class FressianMapDecoder(
    private val obj: Map<*, *>
) : CompositeDecoder {
    private var count: Int = 0
    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        val key = descriptor.getElementName(index)
        return obj[key]?.let { it as Boolean }
            ?: throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
            is Number -> value.toByte()
            else -> throw IllegalArgumentException("$key is not a number, it is ${value::class}")
        }
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        val key = descriptor.getElementName(index)
        return obj[key]?.let { it as Char }
            ?: throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        val key = descriptor.getElementName(index)
        return obj[key]?.let { it as Double }
            ?: throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= obj.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        val key = descriptor.getElementName(index)
        return obj[key]?.let { it as Float}
            ?: throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
            is Number -> value.toInt()
            else -> throw IllegalArgumentException("$key is not a number, it is ${value::class}")
        }
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
            is Number -> value.toLong()
            else -> throw IllegalArgumentException("$key is not a number, it is ${value::class}")
        }
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> null
            else -> deserializer.deserialize(FressianDODecoder(value))
        }
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
            else -> deserializer.deserialize(FressianDODecoder(value))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        val key = descriptor.getElementName(index)
        return when(val value = obj[key]) {
            null -> throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
            is Number -> value.toShort()
            else -> throw IllegalArgumentException("$key is not a number, it is ${value::class}")
        }
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        val key = descriptor.getElementName(index)
        return obj[key]?.let { it as String }
            ?: throw IllegalArgumentException("$key does not exist in deserialized map.  Available keys are: ${obj.keys}")
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }

}

class FressianListDecoder(
    private val obj: List<*>
) : CompositeDecoder {
    private var count: Int = 0

    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        return obj[index] as Boolean
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        return (obj[index] as Number).toByte()
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        return obj[index] as Char
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        return obj[index] as Double
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= obj.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        return obj[index] as Float
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        return (obj[index] as Number).toInt()
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        return (obj[index] as Number).toLong()
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        return when(val value = obj[index]) {
            null -> null
            else -> deserializer.deserialize(FressianDODecoder(value))
        }
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        return when(val value = obj[index]) {
            null -> throw IllegalArgumentException("value at index $index is null")
            else -> deserializer.deserialize(FressianDODecoder(value))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        return (obj[index] as Number).toShort()
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        return obj[index] as String
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }

}

/*
class EdnDecoder(
    private val ednRoot: EdnNode
) : Decoder {

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            when {
                ednRoot is EdnMapNode -> MapDecoder(this, ednRoot)
                ednRoot is EdnTagNode -> MapDecoder(this, ednRoot.node as EdnMapNode)
                else -> throw IllegalArgumentException("Expecting EdnMapNode or EdnTagNode with a map node child, received: ${ednRoot::class}")
            }
        }
        StructureKind.LIST -> {
            when(ednRoot) {
                is EdnListNode -> ListDecoder(this, ednRoot)
                is EdnSetNode -> SetDecoder(this, ednRoot)
                else -> throw IllegalArgumentException("Unsure how to translate ${ednRoot::class} into a list")
            }
        }
        else -> throw IllegalArgumentException("${descriptor.kind} not supported")
    }

    override fun decodeBoolean(): Boolean {
        check(ednRoot is EdnBooleanNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    override fun decodeByte(): Byte {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toByte()
    }

    override fun decodeChar(): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDouble(): Double {
        check(ednRoot is EdnDoubleNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    override fun decodeEnum(enumDescriptor: SerialDescriptor): Int {
        return when(ednRoot) {
            is EdnTagNode -> {
                check(ednRoot.node is EdnStringNode) {
                    "Could not treat tagged node as EdnStringNode, it was ${ednRoot.node::class}"
                }

                enumDescriptor.getElementIndex(ednRoot.node.value).also {
                    check(it != CompositeDecoder.UNKNOWN_NAME) {
                        "Unexpected enum value '${ednRoot.node.value}' for ${ednRoot.tag}"
                    }
                }
            }
            else -> throw IllegalArgumentException("Expected EdnTagNode, received ${ednRoot::class}")
        }
    }

    override fun decodeFloat(): Float {
        check(ednRoot is EdnDoubleNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toFloat()
    }

    override fun decodeInline(descriptor: SerialDescriptor): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeInt(): Int {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toInt()
    }

    override fun decodeLong(): Long {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    @ExperimentalSerializationApi
    override fun decodeNotNullMark(): Boolean {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun decodeNull(): Nothing? {
        TODO("Not yet implemented")
    }

    override fun decodeShort(): Short {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toShort()
    }

    override fun decodeString(): String {
        return when(ednRoot) {
            is EdnStringNode -> ednRoot.value
            is EdnTagNode -> {
                check(ednRoot.node is EdnStringNode) {
                    "Could not treat tagged node as EdnStringNode, it was ${ednRoot.node::class}"
                }
                ednRoot.node.value
            }
            else -> throw IllegalArgumentException("Expected EdnStringNode, received ${ednRoot::class}")
        }
    }
}

class MapDecoder(
    private val parent: EdnDecoder,
    private val ednMapNode: EdnMapNode
) : CompositeDecoder {

    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnBooleanNode -> node.value
            else -> throw IllegalArgumentException("node should be an EdnBooleanNode, it is a ${node::class}")
        }
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value.toByte()
            }
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnDoubleNode -> node.value
            else -> throw IllegalArgumentException("node should be an EdnFloatNode, it is a ${node::class}")
        }
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= descriptor.elementsCount) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnDoubleNode -> node.value.toFloat()
            else -> throw IllegalArgumentException("node should be an EdnFloatNode, it is a ${node::class}")
        }
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val key = ":${descriptor.getElementName(index)}"
        return ednMapNode.value[key]
            ?.let {
                check(it is EdnLongNode) {
                    "Expected EdnLongNode, it is a ${it::class}'"
                }
                it.value.toInt()
            } ?: throw IllegalStateException("$key is not in the map")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value
            }
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        return when (val value = ednMapNode.value[":${descriptor.getElementName(index)}"]) {
            null -> null
            EdnNilNode -> null
            else -> deserializer.deserialize(EdnDecoder(value))
        }
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        return deserializer.deserialize(EdnDecoder(ednMapNode.value[":${descriptor.getElementName(index)}"]!!))
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value.toShort()
            }
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnStringNode)
                it.value
            }
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}

class ListDecoder(
    private val parent: EdnDecoder,
    private val ednListNode: EdnListNode
) : CompositeDecoder {
    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        TODO("Not yet implemented")
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        TODO("Not yet implemented")
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        TODO("Not yet implemented")
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= ednListNode.value.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        TODO("Not yet implemented")
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        TODO("Not yet implemented")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        TODO("Not yet implemented")
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        val node = ednListNode.value[index]

        return if (descriptor.getElementDescriptor(index).kind == SerialKind.CONTEXTUAL) {
            when (node) {
                is EdnBooleanNode -> node.value as T
                is EdnDoubleNode -> node.value as T
                is EdnListNode -> node.value as T
                is EdnLongNode -> node.value as T
                is EdnMapNode -> node.value as T
                EdnNilNode -> throw NullPointerException("null received for non-nullable value")
                is EdnSetNode -> node.value as T
                is EdnStringNode -> node.value as T
                is EdnTagNode -> {
                    when(node.tag) {
                        "#inst" -> InstantSerializer.deserialize(EdnDecoder(node)) as T
                        "#uuid" -> UuidSerializer.deserialize(EdnDecoder(node)) as T
                        else -> throw IllegalArgumentException("Unsure how to deserialized: '${node.tag}'")
                    }

                }
            }
        } else {
            deserializer.deserialize(EdnDecoder(node))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        TODO("Not yet implemented")
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        println("decoding string: $index")
        return "A string"
    }

    override fun endStructure(descriptor: SerialDescriptor) {

    }
}

class SetDecoder(
    private val parent: EdnDecoder,
    private val ednSetNode: EdnSetNode
) : CompositeDecoder {
    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        TODO("Not yet implemented")
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        TODO("Not yet implemented")
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        TODO("Not yet implemented")
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= ednSetNode.value.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        TODO("Not yet implemented")
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        TODO("Not yet implemented")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        TODO("Not yet implemented")
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        val node = ednSetNode.value[index]

        return if (descriptor.getElementDescriptor(index).kind == SerialKind.CONTEXTUAL) {
            when (node) {
                is EdnBooleanNode -> node.value as T
                is EdnDoubleNode -> node.value as T
                is EdnListNode -> node.value as T
                is EdnLongNode -> node.value as T
                is EdnMapNode -> node.value as T
                EdnNilNode -> throw NullPointerException("null received for non-nullable value")
                is EdnSetNode -> node.value as T
                is EdnStringNode -> node.value as T
                is EdnTagNode -> {
                    when(node.tag) {
                        "#inst" -> InstantSerializer.deserialize(EdnDecoder(node)) as T
                        "#uuid" -> UuidSerializer.deserialize(EdnDecoder(node)) as T
                        else -> throw IllegalArgumentException("Unsure how to deserialized: '${node.tag}'")
                    }

                }
            }
        } else {
            deserializer.deserialize(EdnDecoder(node))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        TODO("Not yet implemented")
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        println("decoding string: $index")
        return "A string"
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}
 */