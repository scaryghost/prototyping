package scaryghost.ktstdlib.ktserialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.CompositeEncoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual
import kotlinx.serialization.modules.plus
import kotlinx.serialization.modules.serializersModuleOf
import org.fressian.Reader
import org.fressian.Writer
import org.fressian.handlers.ReadHandler
import org.fressian.handlers.WriteHandler
import java.time.Instant
import java.util.*

// https://github.com/clojure/data.fressian/blob/master/src/main/clojure/clojure/data/fressian.clj#L71

class FressianDOEncoder : Encoder {
    companion object {
        val tags = mutableMapOf<String, String>(
            Instant::class.java.canonicalName to "inst",
            UUID::class.java.canonicalName to "uuid"
        )
        var translators: SerializersModule =
            serializersModuleOf(object: KSerializer<Instant> {
                override val descriptor: SerialDescriptor
                    get() = buildClassSerialDescriptor("java.time.Instant") {
                        element<Long>("seconds")
                        element<Int>("nanos")
                    }

                override fun deserialize(decoder: Decoder): Instant {
                    TODO("Not yet implemented")
                }

                override fun serialize(encoder: Encoder, value: Instant) {
                    check(encoder is FressianDOEncoder) {
                        "This decode is only to be used with FressianDOEncoder"
                    }

                    when(encoder.type) {
                        BaseType.MAP -> {
                            val k = encoder.key
                            checkNotNull(k) {
                                "Need to set key before serializing its boolean value: $value"
                            }
                            encoder.entriesAsMap[k] = Date.from(value)

                            encoder.key = null
                        }
                        BaseType.LIST -> {
                            encoder.entriesAsList.add(Date.from(value))
                        }
                    }
                }

            }) + serializersModuleOf(object: KSerializer<UUID> {
                override val descriptor: SerialDescriptor
                    get() = buildClassSerialDescriptor("java.util.UUID") {
                        element<Long>("mostSigBits")
                        element<Int>("leastSigBits")
                    }

                override fun deserialize(decoder: Decoder): UUID {
                    TODO("Not yet implemented")
                }

                override fun serialize(encoder: Encoder, value: UUID) {
                    check(encoder is FressianDOEncoder) {
                        "This decode is only to be used with FressianDOEncoder"
                    }

                    when(encoder.type) {
                        BaseType.MAP -> {
                            val k = encoder.key
                            checkNotNull(k) {
                                "Need to set key before serializing its boolean value: $value"
                            }
                            encoder.entriesAsMap[k] = value

                            encoder.key = null
                        }
                        BaseType.LIST -> {
                            encoder.entriesAsList.add(value)
                        }
                    }
                }

            })

        inline fun <reified T : Any> registerTranslator(serializer: KSerializer<T>) {
            tags[serializer.descriptor.serialName] = serializer.descriptor.serialName
            translators += SerializersModule {
                contextual(serializer)
            }
        }
    }

    enum class BaseType {
        MAP,
        LIST
    }

    lateinit var type: BaseType

    val entriesAsMap = mutableMapOf<Any, Any?>()
    var key: Any? = null

    val entriesAsList = mutableListOf<Any?>()
    var scalarEntry: Any? = null


    override val serializersModule: SerializersModule
        get() = translators

    override fun beginStructure(descriptor: SerialDescriptor): CompositeEncoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            type = BaseType.MAP
            FressianMapEncoder(this)
        }
        StructureKind.LIST -> {
            type = BaseType.LIST
            FressianListEncoder(this)
        }
        else -> TODO("Not yet implemented")
    }

    override fun encodeBoolean(value: Boolean) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its boolean value: $value"
                }
                entriesAsMap[k] = value

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value)
            }
        }
    }

    override fun encodeByte(value: Byte) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its byte value: $value"
                }
                entriesAsMap[k] = value.toLong()

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value.toLong())
            }
        }
    }

    override fun encodeChar(value: Char) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its char value: $value"
                }
                entriesAsMap[k] = value

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value)
            }
        }
    }

    override fun encodeDouble(value: Double) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its double value: $value"
                }
                entriesAsMap[k] = value

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value)
            }
        }
    }

    override fun encodeEnum(enumDescriptor: SerialDescriptor, index: Int) {
        val enumName = enumDescriptor.getElementName(index)

        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its enum value: $enumName"
                }
                entriesAsMap[k] = "#${enumDescriptor.serialName} \"$enumName\""

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add("#${enumDescriptor.serialName} \"$enumName\"")
            }
        }
    }

    override fun encodeFloat(value: Float) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its float value: $value"
                }
                entriesAsMap[k] = value.toDouble()

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value.toDouble())
            }
        }
    }

    override fun encodeInline(descriptor: SerialDescriptor): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeInt(value: Int) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its int value: $value"
                }
                entriesAsMap[k] = value.toLong()

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value.toLong())
            }
        }
    }

    override fun encodeLong(value: Long) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its null value: $value"
                }
                entriesAsMap[k] = value

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value)
            }
        }
    }

    @ExperimentalSerializationApi
    override fun encodeNull() {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing a null value"
                }
                entriesAsMap[k] = null

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(null)
            }
        }
    }

    override fun encodeShort(value: Short) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its short value: $value"
                }
                entriesAsMap[k] = value.toLong()

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value.toLong())
            }
        }
    }

    override fun encodeString(value: String) {
        when(type) {
            BaseType.MAP -> {
                val k = key
                checkNotNull(k) {
                    "Need to set key before serializing its string value: $value"
                }
                entriesAsMap[k] = value

                key = null
            }
            BaseType.LIST -> {
                entriesAsList.add(value)
            }
        }
    }
}


class FressianMapEncoder(
    private val parent: FressianDOEncoder,
) : CompositeEncoder {
    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun encodeBooleanElement(descriptor: SerialDescriptor, index: Int, value: Boolean) {
        parent.entriesAsList
    }

    override fun encodeByteElement(descriptor: SerialDescriptor, index: Int, value: Byte) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeByte(value)
    }

    override fun encodeCharElement(descriptor: SerialDescriptor, index: Int, value: Char) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeChar(value)
    }

    override fun encodeDoubleElement(descriptor: SerialDescriptor, index: Int, value: Double) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeDouble(value)
    }

    override fun encodeFloatElement(descriptor: SerialDescriptor, index: Int, value: Float) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeFloat(value)
    }

    override fun encodeInlineElement(descriptor: SerialDescriptor, index: Int): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeIntElement(descriptor: SerialDescriptor, index: Int, value: Int) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeInt(value)
    }

    override fun encodeLongElement(descriptor: SerialDescriptor, index: Int, value: Long) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeLong(value)
    }

    @ExperimentalSerializationApi
    override fun <T : Any> encodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T?) {
        if (value != null) {
            parent.key = EdnKeyword(descriptor.getElementName(index))
            parent.encodeSerializableValue(serializer, value)
        }
    }

    override fun <T> encodeSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeSerializableValue(serializer, value)
    }

    override fun encodeShortElement(descriptor: SerialDescriptor, index: Int, value: Short) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeShort(value)
    }

    override fun encodeStringElement(descriptor: SerialDescriptor, index: Int, value: String) {
        parent.key = EdnKeyword(descriptor.getElementName(index))
        parent.encodeString(value)
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}

class FressianListEncoder(
    private val parent: FressianDOEncoder,
) : CompositeEncoder {
    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun encodeBooleanElement(descriptor: SerialDescriptor, index: Int, value: Boolean) {
        parent.encodeBoolean(value)
    }

    override fun encodeByteElement(descriptor: SerialDescriptor, index: Int, value: Byte) {
        parent.encodeByte(value)
    }

    override fun encodeCharElement(descriptor: SerialDescriptor, index: Int, value: Char) {
        parent.encodeChar(value)
    }

    override fun encodeDoubleElement(descriptor: SerialDescriptor, index: Int, value: Double) {
        parent.encodeDouble(value)
    }

    override fun encodeFloatElement(descriptor: SerialDescriptor, index: Int, value: Float) {
        parent.encodeFloat(value)
    }

    override fun encodeInlineElement(descriptor: SerialDescriptor, index: Int): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeIntElement(descriptor: SerialDescriptor, index: Int, value: Int) {
        parent.encodeInt(value)
    }

    override fun encodeLongElement(descriptor: SerialDescriptor, index: Int, value: Long) {
        parent.encodeLong(value)
    }

    @ExperimentalSerializationApi
    override fun <T : Any> encodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T?) {
        when(value) {
            null -> parent.encodeNull()
            else -> encodeSerializableElement(serializer, value)
        }
    }

    override fun <T> encodeSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T) {
        encodeSerializableElement(serializer, value)
    }

    private fun <T> encodeSerializableElement(serializer: SerializationStrategy<T>, value: T) {
        parent.encodeSerializableValue(serializer, value)
    }

    override fun encodeShortElement(descriptor: SerialDescriptor, index: Int, value: Short) {
        parent.encodeShort(value)
    }

    override fun encodeStringElement(descriptor: SerialDescriptor, index: Int, value: String) {
        parent.encodeString(value)
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}

@JvmInline
value class EdnKeyword(val value: String)

object EdnWriteKeyHandler : WriteHandler {
    override fun write(w: Writer, instance: Any) {
        w.writeTag("key", 2)

        check(instance is EdnKeyword)
        val parts = instance.value.split("/")
        when(parts.size) {
            1 -> {
                w.writeNull()
                w.writeObject(parts[0])
            }
            2 -> {
                w.writeObject(parts[0])
                w.writeObject(parts[1])
            }
            else -> throw IllegalArgumentException("${instance.value} does not appear to be a valid keyword")
        }
    }
}

class EdnWriteMapHandler : WriteHandler {
    override fun write(w: Writer, instance: Any) {
        w.writeTag("map", 1)

        check(instance is Map<*, *>)
        instance.forEach { (k, v) ->
            w.writeObject(k, true)
            w.writeObject(v)
        }
    }
}
