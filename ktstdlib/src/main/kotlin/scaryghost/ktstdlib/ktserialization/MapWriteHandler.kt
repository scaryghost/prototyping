package scaryghost.ktstdlib.ktserialization

import com.github.rschmitt.dynamicobject.DynamicObject
import org.fressian.CachedObject
import org.fressian.Writer
import org.fressian.handlers.WriteHandler
import java.util.*

// adopted from DynamicObject
// https://github.com/rschmitt/dynamic-object/blob/master/src/main/java/com/github/rschmitt/dynamicobject/FressianWriteHandler.java
class MapWriteHandler<T>(
    type: Class<T>,
    private val tag: String,
) : WriteHandler {
    private val cachedKeys: Set<Any> = mutableSetOf()
    override fun write(writer: Writer, instance: Any) {


        // We manually serialize the backing map so that we can apply caching transformations to specific subcomponents.
        // To avoid needless copying we do this via an adapter rather than copying to a temporary list.
        writer.writeTag(tag, 1)
        writer.writeTag("map", 1)

        val map = instance as Map<Any, Any>
        writer.writeList(TransformedMap<T>(map, this::transformKey, this::transformValue))
    }

    private fun transformKey(key: Any): Any {
        return CachedObject(key)
    }

    private fun transformValue(key: Any, value: Any): Any {
        if (cachedKeys.contains(key)) {
            return CachedObject(value)
        }

        return value
    }
}

class TransformedMap<T>(
    private val backingMap: Map<Any, Any>,
    private val keysTransformation: (Any) -> Any,
    private val valuesTransformation: (Any, Any) -> Any,
) : AbstractCollection<T>() {
    override fun iterator(): MutableIterator<T> {
        return TransformingKeyValueIterator(backingMap.entries.iterator(), keysTransformation, valuesTransformation)
    }

    override val size: Int
        get() = backingMap.size * 2
}

class TransformingKeyValueIterator<T>(
    private val entryIterator: Iterator<Map.Entry<Any, Any>>,
    private val keysTransformation: (Any) -> Any,
    private val valuesTransformation: (Any, Any) -> Any,
) : MutableIterator<T> {

    var pendingValue: Any? = null
    var hasPendingValue: Boolean = false
    override fun hasNext(): Boolean {
        return hasPendingValue || entryIterator.hasNext()
    }

    override fun next(): T {
        if (hasPendingValue) {
            val value = pendingValue
            pendingValue = null
            hasPendingValue = false

            return value!! as T
        }

        val entry = entryIterator.next()
        pendingValue = valuesTransformation(entry.key, entry.value)

        hasPendingValue = true

        return keysTransformation(entry.key) as T
    }

    override fun remove() {
        TODO("Not yet implemented")
    }
}