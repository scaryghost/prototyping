package scaryghost.ktstdlib.ktserialization

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.SerialKind
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.CompositeDecoder.Companion.DECODE_DONE
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.plus
import kotlinx.serialization.modules.serializersModuleOf
import kotlinx.serialization.serializer
import java.io.*

const val EDN = """{
  :name "ZGMF-X19A \u221E Justice", 
  :pilots #{
    {:firstName "Lacus", :lastName "Clyne"}, 
    {:firstName "Athrun", :lastName "Zala"}
  }, 
  :height 18.9, 
  :weight 79.67, 
  :meteor-compatible true, 
  :debut #inst "2005-08-06T09:22:20Z", 
  :armaments [
    {:name "MMI-GAU26 17.5mm CIWS", :count 4, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MMI-M19L 14mm Twin CIWS", :count 2, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MA-M02G \"Super Lacerta\" Beam Saber", :count 2, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MA-M1911 High-energy Beam Rifle", :count 1, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MR-Q15A \"Griffon\" Beam Blade", :count 2, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MA-6J \"Hyper Fortis\" Beam Cannon", :count 2, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "FIXED"}, 
    {:name "MA-M1911 High-energy Beam Rifle", :count 1, :placement #scaryghost.ktstdlib.ktserialization.Armament.Placement "HANDHELD"}
  ]
}"""

/*
MobileSuit(
  name=ZGMF-X19A ∞ Justice,
  pilots=[
    Pilot(firstName=Lacus, lastName=Clyne),
    Pilot(firstName=Athrun, lastName=Zala)
  ],
  height=18.9,
  weight=79.67,
  meteorCompatible=true,
  debut=2005-08-06T09:22:20Z,
  armaments=[
    Armament(name=MMI-GAU26 17.5mm CIWS, count=4, placement=FIXED),
    Armament(name=MMI-M19L 14mm Twin CIWS, count=2, placement=FIXED),
    Armament(name=MA-M02G "Super Lacerta" Beam Saber, count=2, placement=FIXED),
    Armament(name=MA-M1911 High-energy Beam Rifle, count=1, placement=FIXED),
    Armament(name=MR-Q15A "Griffon" Beam Blade, count=2, placement=FIXED),
    Armament(name=MA-6J "Hyper Fortis" Beam Cannon, count=2, placement=FIXED),
    Armament(name=MA-M1911 High-energy Beam Rifle, count=1, placement=HANDHELD)
  ]
)
 */
fun main(args: Array<String>) {
    val tokenizer = EdnTokenizer(StringReader(EDN))

    val decoder = EdnDecoder(tokenizer.decodeRoot())
    val copy = decoder.decodeSerializableValue<MobileSuit>(serializer())

    println(copy)
}

class EdnDecoder(
    private val ednRoot: EdnNode
) : Decoder {

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            when {
                ednRoot is EdnMapNode -> MapDecoder(this, ednRoot)
                ednRoot is EdnTagNode -> MapDecoder(this, ednRoot.node as EdnMapNode)
                else -> throw IllegalArgumentException("Expecting EdnMapNode or EdnTagNode with a map node child, received: ${ednRoot::class}")
            }
        }
        StructureKind.LIST -> {
            when(ednRoot) {
                is EdnListNode -> ListDecoder(this, ednRoot)
                is EdnSetNode -> SetDecoder(this, ednRoot)
                else -> throw IllegalArgumentException("Unsure how to translate ${ednRoot::class} into a list")
            }
        }
        else -> throw IllegalArgumentException("${descriptor.kind} not supported")
    }

    override fun decodeBoolean(): Boolean {
        check(ednRoot is EdnBooleanNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    override fun decodeByte(): Byte {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toByte()
    }

    override fun decodeChar(): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDouble(): Double {
        check(ednRoot is EdnDoubleNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    override fun decodeEnum(enumDescriptor: SerialDescriptor): Int {
        return when(ednRoot) {
            is EdnTagNode -> {
                check(ednRoot.node is EdnStringNode) {
                    "Could not treat tagged node as EdnStringNode, it was ${ednRoot.node::class}"
                }

                enumDescriptor.getElementIndex(ednRoot.node.value).also {
                    check(it != CompositeDecoder.UNKNOWN_NAME) {
                        "Unexpected enum value '${ednRoot.node.value}' for ${ednRoot.tag}"
                    }
                }
            }
            else -> throw IllegalArgumentException("Expected EdnTagNode, received ${ednRoot::class}")
        }
    }

    override fun decodeFloat(): Float {
        check(ednRoot is EdnDoubleNode) {
            "Expecting EdnDoubleNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toFloat()
    }

    override fun decodeInline(descriptor: SerialDescriptor): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeInt(): Int {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toInt()
    }

    override fun decodeLong(): Long {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value
    }

    @ExperimentalSerializationApi
    override fun decodeNotNullMark(): Boolean {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun decodeNull(): Nothing? {
        TODO("Not yet implemented")
    }

    override fun decodeShort(): Short {
        check(ednRoot is EdnLongNode) {
            "Expecting EdnLongNode, received '${ednRoot::class}'"
        }

        return ednRoot.value.toShort()
    }

    override fun decodeString(): String {
        return when(ednRoot) {
            is EdnStringNode -> ednRoot.value
            is EdnTagNode -> {
                check(ednRoot.node is EdnStringNode) {
                    "Could not treat tagged node as EdnStringNode, it was ${ednRoot.node::class}"
                }
                ednRoot.node.value
            }
            else -> throw IllegalArgumentException("Expected EdnStringNode, received ${ednRoot::class}")
        }
    }
}

class MapDecoder(
    private val parent: EdnDecoder,
    private val ednMapNode: EdnMapNode
) : CompositeDecoder {

    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnBooleanNode -> node.value
            else -> throw IllegalArgumentException("node should be an EdnBooleanNode, it is a ${node::class}")
        }
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value.toByte()
            }
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnDoubleNode -> node.value
            else -> throw IllegalArgumentException("node should be an EdnFloatNode, it is a ${node::class}")
        }
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= descriptor.elementsCount) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        return when(val node = ednMapNode.value[":${descriptor.getElementName(index)}"]!!) {
            is EdnDoubleNode -> node.value.toFloat()
            else -> throw IllegalArgumentException("node should be an EdnFloatNode, it is a ${node::class}")
        }
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        val key = ":${descriptor.getElementName(index)}"
        return ednMapNode.value[key]
            ?.let {
                check(it is EdnLongNode) {
                    "Expected EdnLongNode, it is a ${it::class}'"
                }
                it.value.toInt()
            } ?: throw IllegalStateException("$key is not in the map")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value
            }
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        return when (val value = ednMapNode.value[":${descriptor.getElementName(index)}"]) {
            null -> null
            EdnNilNode -> null
            else -> deserializer.deserialize(EdnDecoder(value))
        }
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        return deserializer.deserialize(EdnDecoder(ednMapNode.value[":${descriptor.getElementName(index)}"]!!))
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnLongNode)
                it.value.toShort()
            }
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        return ednMapNode.value[":${descriptor.getElementName(index)}"]
            .let {
                check(it is EdnStringNode)
                it.value
            }
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}

class ListDecoder(
    private val parent: EdnDecoder,
    private val ednListNode: EdnListNode
) : CompositeDecoder {
    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        TODO("Not yet implemented")
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        TODO("Not yet implemented")
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        TODO("Not yet implemented")
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= ednListNode.value.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        TODO("Not yet implemented")
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        TODO("Not yet implemented")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        TODO("Not yet implemented")
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        val node = ednListNode.value[index]

        return if (descriptor.getElementDescriptor(index).kind == SerialKind.CONTEXTUAL) {
            when (node) {
                is EdnBooleanNode -> node.value as T
                is EdnDoubleNode -> node.value as T
                is EdnListNode -> node.value as T
                is EdnLongNode -> node.value as T
                is EdnMapNode -> node.value as T
                EdnNilNode -> throw NullPointerException("null received for non-nullable value")
                is EdnSetNode -> node.value as T
                is EdnStringNode -> node.value as T
                is EdnTagNode -> {
                    when(node.tag) {
                        "#inst" -> InstantSerializer.deserialize(EdnDecoder(node)) as T
                        "#uuid" -> UuidSerializer.deserialize(EdnDecoder(node)) as T
                        else -> throw IllegalArgumentException("Unsure how to deserialized: '${node.tag}'")
                    }

                }
            }
        } else {
            deserializer.deserialize(EdnDecoder(node))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        TODO("Not yet implemented")
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        println("decoding string: $index")
        return "A string"
    }

    override fun endStructure(descriptor: SerialDescriptor) {

    }
}

class SetDecoder(
    private val parent: EdnDecoder,
    private val ednSetNode: EdnSetNode
) : CompositeDecoder {
    private var count: Int = 0

    override val serializersModule: SerializersModule = serializersModuleOf(InstantSerializer)

    override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean {
        TODO("Not yet implemented")
    }

    override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte {
        TODO("Not yet implemented")
    }

    override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char {
        TODO("Not yet implemented")
    }

    override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double {
        TODO("Not yet implemented")
    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return when(count >= ednSetNode.value.size) {
            true -> DECODE_DONE
            else -> count++
        }
    }

    override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float {
        TODO("Not yet implemented")
    }

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder {
        TODO("Not yet implemented")
    }

    override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        TODO("Not yet implemented")
    }

    override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> decodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T?>, previousValue: T?): T? {
        TODO("Not yet implemented")
    }

    override fun <T> decodeSerializableElement(descriptor: SerialDescriptor, index: Int, deserializer: DeserializationStrategy<T>, previousValue: T?): T {
        val node = ednSetNode.value[index]

        return if (descriptor.getElementDescriptor(index).kind == SerialKind.CONTEXTUAL) {
            when (node) {
                is EdnBooleanNode -> node.value as T
                is EdnDoubleNode -> node.value as T
                is EdnListNode -> node.value as T
                is EdnLongNode -> node.value as T
                is EdnMapNode -> node.value as T
                EdnNilNode -> throw NullPointerException("null received for non-nullable value")
                is EdnSetNode -> node.value as T
                is EdnStringNode -> node.value as T
                is EdnTagNode -> {
                    when(node.tag) {
                        "#inst" -> InstantSerializer.deserialize(EdnDecoder(node)) as T
                        "#uuid" -> UuidSerializer.deserialize(EdnDecoder(node)) as T
                        else -> throw IllegalArgumentException("Unsure how to deserialized: '${node.tag}'")
                    }

                }
            }
        } else {
            deserializer.deserialize(EdnDecoder(node))
        }
    }

    override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short {
        TODO("Not yet implemented")
    }

    override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String {
        println("decoding string: $index")
        return "A string"
    }

    override fun endStructure(descriptor: SerialDescriptor) {
    }
}