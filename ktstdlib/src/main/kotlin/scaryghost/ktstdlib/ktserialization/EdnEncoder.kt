package scaryghost.ktstdlib.ktserialization

import com.github.rschmitt.dynamicobject.DynamicObject
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.StructureKind
import kotlinx.serialization.encoding.CompositeEncoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual
import kotlinx.serialization.modules.plus
import kotlinx.serialization.modules.serializersModuleOf
import kotlinx.serialization.serializer
import org.apache.commons.text.StringEscapeUtils.escapeJava
import scaryghost.ktstdlib.ktserialization.Armament.Placement
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.time.Instant
import java.util.*

fun main(args: Array<String>) {
    val ms = MobileSuit(
        name = "ZGMF-X19A ∞ Justice",
        pilots = setOf(
            Pilot("Lacus", "Clyne"),
            Pilot("Athrun", "Zala"),
        ),
    debut = Calendar.Builder()
        .setDate(2005, Calendar.AUGUST, 6)
        .setTimeOfDay(18, 22, 20)
        .setTimeZone(TimeZone.getTimeZone("GMT+9"))
        .build()
        .toInstant(),
    height = 18.9f,
    weight = 79.67f,
    meteorCompatible = true,
    armaments = listOf(
        4 to "MMI-GAU26 17.5mm CIWS",
        2 to "MMI-M19L 14mm Twin CIWS",
        2 to "MA-M02G \"Super Lacerta\" Beam Saber",
        1 to "MA-M1911 High-energy Beam Rifle",
        2 to "MR-Q15A \"Griffon\" Beam Blade",
        2 to "MA-6J \"Hyper Fortis\" Beam Cannon"
    ).map { (count, name) ->
        Armament(name, count, Placement.FIXED)
    } + listOf(
        1 to "MA-M1911 High-energy Beam Rifle",
    ).map { (count, name) ->
        Armament(name, count, Placement.HANDHELD)
    }
    )

    val baos = ByteArrayOutputStream()
    val encoder = EdnEncoder(baos)
    encoder.encodeSerializableValue(serializer(), ms)
    encoder.writer.flush()
    encoder.writer.close()

    val edn = baos.toString()
    println(edn)

    val dynamicObjectRepr = DynamicObject.deserialize(edn, scaryghost.ktstdlib.edn.MobileSuit::class.java)
    dynamicObjectRepr.prettyPrint()
}

class EdnEncoder(
    os: OutputStream
) : Encoder{
    companion object {
        val tags = mutableMapOf<String, String>(
            Instant::class.java.canonicalName to "inst",
            UUID::class.java.canonicalName to "uuid"
        )
        var translators: SerializersModule =
            serializersModuleOf(InstantSerializer) + serializersModuleOf(UuidSerializer)

        inline fun <reified T : Any> registerTranslator(serializer: KSerializer<T>) {
            tags[serializer.descriptor.serialName] = serializer.descriptor.serialName
            translators += SerializersModule {
                contextual(serializer)
            }
        }
    }

    val writer = OutputStreamWriter(os)
    override val serializersModule: SerializersModule
        get() = translators

    override fun beginStructure(descriptor: SerialDescriptor): CompositeEncoder = when(descriptor.kind) {
        StructureKind.CLASS -> {
            EdnMapEncoder(this)
        }
        StructureKind.LIST -> {
            when {
                "List" in descriptor.serialName -> EdnListEncoder(this, "[", "]")
                "Set" in descriptor.serialName -> EdnListEncoder(this, "#{", "}")
                else -> throw IllegalArgumentException("${descriptor.serialName} is not a supported list type")
            }

        }
        else -> TODO("Not yet implemented")
    }

    override fun encodeBoolean(value: Boolean) {
        writer.write("$value")
    }

    override fun encodeByte(value: Byte) {
        writer.write("$value")
    }

    override fun encodeChar(value: Char) {
        TODO("Not yet implemented")
    }

    override fun encodeDouble(value: Double) {
        writer.write("$value")
    }

    override fun encodeEnum(enumDescriptor: SerialDescriptor, index: Int) {
        writer.write("#${enumDescriptor.serialName} \"${enumDescriptor.getElementName(index)}\"")
    }

    override fun encodeFloat(value: Float) {
        writer.write("$value")
    }

    override fun encodeInline(descriptor: SerialDescriptor): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeInt(value: Int) {
        writer.write("$value")
    }

    override fun encodeLong(value: Long) {
        writer.write("$value")
    }

    @ExperimentalSerializationApi
    override fun encodeNull() {
        writer.write("nil")
    }

    override fun <T> encodeSerializableValue(serializer: SerializationStrategy<T>, value: T) {
        tags[serializer.descriptor.serialName]
            ?.also { writer.write("#$it ") }
        serializer.serialize(this, value)
    }

    override fun encodeShort(value: Short) {
        writer.write("$value")
    }

    override fun encodeString(value: String) {
        writer.write("\"")
        writer.write(escapeJava(value))
        writer.write("\"")
    }
}

class EdnMapEncoder(
    private val parent: EdnEncoder,
) : CompositeEncoder {
    init {
        parent.writer.write("{")
    }

    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun encodeBooleanElement(descriptor: SerialDescriptor, index: Int, value: Boolean) {
        insertKey(descriptor, index)
        parent.encodeBoolean(value)
    }

    override fun encodeByteElement(descriptor: SerialDescriptor, index: Int, value: Byte) {
        insertKey(descriptor, index)
        parent.encodeByte(value)
    }

    override fun encodeCharElement(descriptor: SerialDescriptor, index: Int, value: Char) {
        TODO("Not yet implemented")
    }

    override fun encodeDoubleElement(descriptor: SerialDescriptor, index: Int, value: Double) {
        insertKey(descriptor, index)
        parent.encodeDouble(value)
    }

    override fun encodeFloatElement(descriptor: SerialDescriptor, index: Int, value: Float) {
        insertKey(descriptor, index)
        parent.encodeFloat(value)
    }

    override fun encodeInlineElement(descriptor: SerialDescriptor, index: Int): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeIntElement(descriptor: SerialDescriptor, index: Int, value: Int) {
        insertKey(descriptor, index)
        parent.encodeInt(value)
    }

    override fun encodeLongElement(descriptor: SerialDescriptor, index: Int, value: Long) {
        insertKey(descriptor, index)
        parent.encodeLong(value)
    }

    @ExperimentalSerializationApi
    override fun <T : Any> encodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T?) {
        insertKey(descriptor, index)

        when(value) {
            null -> parent.encodeNull()
            else -> encodeSerializableElement(serializer, value)
        }
    }

    override fun <T> encodeSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T) {
        insertKey(descriptor, index)

        encodeSerializableElement(serializer, value)
    }

    private fun <T> encodeSerializableElement(serializer: SerializationStrategy<T>, value: T) {
        parent.encodeSerializableValue(serializer, value)
    }

    override fun encodeShortElement(descriptor: SerialDescriptor, index: Int, value: Short) {
        insertKey(descriptor, index)
        parent.encodeShort(value)
    }

    override fun encodeStringElement(descriptor: SerialDescriptor, index: Int, value: String) {
        insertKey(descriptor, index)
        parent.encodeString(value)
    }

    override fun endStructure(descriptor: SerialDescriptor) {
        parent.writer.write("}")
    }

    private fun insertKey(descriptor: SerialDescriptor, index: Int) {
        val key = descriptor.getElementName(index)

        if (index != 0) {
            parent.writer.write(", ")
        }
        parent.writer.write(":$key ")
    }
}

class EdnListEncoder(
    private val parent: EdnEncoder,
    openingChar: String,
    private val closingChar: String
) : CompositeEncoder {
    init {
        parent.writer.write(openingChar)
    }

    override val serializersModule: SerializersModule
        get() = TODO("Not yet implemented")

    override fun encodeBooleanElement(descriptor: SerialDescriptor, index: Int, value: Boolean) {
        TODO("Not yet implemented")
    }

    override fun encodeByteElement(descriptor: SerialDescriptor, index: Int, value: Byte) {
        TODO("Not yet implemented")
    }

    override fun encodeCharElement(descriptor: SerialDescriptor, index: Int, value: Char) {
        TODO("Not yet implemented")
    }

    override fun encodeDoubleElement(descriptor: SerialDescriptor, index: Int, value: Double) {
        TODO("Not yet implemented")
    }

    override fun encodeFloatElement(descriptor: SerialDescriptor, index: Int, value: Float) {
        TODO("Not yet implemented")
    }

    override fun encodeInlineElement(descriptor: SerialDescriptor, index: Int): Encoder {
        TODO("Not yet implemented")
    }

    override fun encodeIntElement(descriptor: SerialDescriptor, index: Int, value: Int) {
        TODO("Not yet implemented")
    }

    override fun encodeLongElement(descriptor: SerialDescriptor, index: Int, value: Long) {
        TODO("Not yet implemented")
    }

    @ExperimentalSerializationApi
    override fun <T : Any> encodeNullableSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T?) {
        insertComma(descriptor, index)

        when(value) {
            null -> parent.encodeNull()
            else -> encodeSerializableElement(serializer, value)
        }
    }

    override fun <T> encodeSerializableElement(descriptor: SerialDescriptor, index: Int, serializer: SerializationStrategy<T>, value: T) {
        insertComma(descriptor, index)
        encodeSerializableElement(serializer, value)
    }

    private fun <T> encodeSerializableElement(serializer: SerializationStrategy<T>, value: T) {
        parent.encodeSerializableValue(serializer, value)
    }

    override fun encodeShortElement(descriptor: SerialDescriptor, index: Int, value: Short) {
        TODO("Not yet implemented")
    }

    override fun encodeStringElement(descriptor: SerialDescriptor, index: Int, value: String) {
        TODO("Not yet implemented")
    }

    override fun endStructure(descriptor: SerialDescriptor) {
        parent.writer.write(closingChar)
    }

    private fun insertComma(descriptor: SerialDescriptor, index: Int) {
        if (index != 0) {
            parent.writer.write(", ")
        }
    }
}