package scaryghost.ktstdlib.ktserialization

import java.io.PushbackReader
import java.io.Reader

class EdnTokenizer(
    reader: Reader
) {
    private val reader = PushbackReader(reader)
    private val tokenBuilder = StringBuilder()

    tailrec fun next(): EdnToken {
        return when(val ch = nextChar()) {
            in '0' .. '9' -> {
                tokenBuilder.append(ch)
                readNumber()
                    .also { tokenBuilder.setLength(0) }
            }
            '{' -> EdnSymbolToken("{")
            '}' -> EdnSymbolToken("}")
            '[' -> EdnSymbolToken("[")
            ']' -> EdnSymbolToken("]")
            '+' -> readNumber()
                .also { tokenBuilder.setLength(0) }
            '-' -> {
                tokenBuilder.append(ch)
                readNumber()
                    .also { tokenBuilder.setLength(0) }
            }
            '\"' -> readString()
                .also { tokenBuilder.setLength(0) }
            ':' -> {
                tokenBuilder.append(ch)
                readKeyword()
                    .also { tokenBuilder.setLength(0) }
            }
            '#' -> {
                tokenBuilder.append(ch)
                nextSetOrTaggedElement()
            }
            else -> {
                when {
                    ch.isEdnWhitespace -> next()
                    Character.isLetter(ch) -> {
                        tokenBuilder.append(ch)
                        readSpecialWord()
                            .also { tokenBuilder.setLength(0) }
                    }
                    else -> throw IllegalArgumentException("Unrecognized EDN character: '$ch'")
                }
            }
        }
    }

    private fun nextSetOrTaggedElement(): EdnToken {
        val ch = nextChar()

        return when {
            ch == '{' -> {
                tokenBuilder.append(ch)
                EdnSymbolToken(tokenBuilder.toString())
                    .also { tokenBuilder.setLength(0) }
            }
            Character.isLetter(ch) -> {
                tokenBuilder.append(ch)
                readTag()
                    .also { tokenBuilder.setLength(0) }
            }
            else -> throw IllegalArgumentException("Unrecognized EDN sequence: '$tokenBuilder'")
        }
    }

    private fun nextChar(): Char {
        return when(val code = reader.read()) {
            -1 -> throw NoSuchElementException("Mo more characters to read")
            else -> code.toChar()
        }
    }

    private fun readTag(): EdnTagToken {
        while(true) {
            val ch = try {
                nextChar()
            } catch (_: NoSuchElementException) {
                break
            }

            if (ch.isEdnWhitespace || ch == '[' || ch == '{' || ch == ']' || ch == '}' || ch == '\"') {
                reader.unread(ch.code)
                break
            }

            tokenBuilder.append(ch)
        }

        return EdnTagToken(tokenBuilder.toString())
    }

    private fun readKeyword(): EdnKeywordToken {
        while(true) {
            val ch = try {
                nextChar()
            } catch (_: NoSuchElementException) {
                break
            }

            if (ch.isEdnWhitespace) {
                reader.unread(ch.code)
                break
            }

            tokenBuilder.append(ch)
        }

        return EdnKeywordToken(tokenBuilder.toString())
    }

    private fun readNumber(): EdnToken {
        val readDigits = {
            while(true) {
                val ch = try {
                    nextChar()
                } catch (_: NoSuchElementException) {
                    break
                }

                if (ch.isEdnWhitespace) {
                    break
                }

                if (Character.isDigit(ch)) {
                    tokenBuilder.append(ch)
                } else {
                    reader.unread(ch.code)

                    if (ch == ']' || ch == '}') {
                        break
                    }
                    throw IllegalArgumentException("Non-digit character read: '$ch'")
                }
            }
        }

        try {
            readDigits()
            return EdnLongToken(tokenBuilder.toString())
        } catch (t: IllegalArgumentException) {
            var nonDigit = nextChar()

            if (nonDigit == '.') {
                tokenBuilder.append(nonDigit)

                try {
                    readDigits()
                    return EdnDoubleToken(tokenBuilder.toString())
                } catch (_: IllegalArgumentException) {
                    nonDigit = nextChar()
                }
            }

            if (nonDigit == 'E' || nonDigit == 'e') {
                tokenBuilder.append(nonDigit)

                nextChar().also {
                    if (it == '-') {
                        tokenBuilder.append(it)
                    } else {
                        reader.unread(it.code)
                    }
                }

                readDigits()
                return EdnDoubleToken(tokenBuilder.toString())
            }

            if (tokenBuilder.length > 1) {
                println("my non-digit: '$nonDigit'")
                throw IllegalArgumentException("Not a valid number: '$tokenBuilder$nonDigit'")
            }

            reader.unread(nonDigit.code)
            return readSpecialWord()
        }
    }

    private fun readString(): EdnStringToken {
        while(true) {
            val ch = try {
                nextChar()
            } catch (_: NoSuchElementException) {
                break
            }

            if (ch == '\\') {
                tokenBuilder.append(
                    when(val nextCh = reader.read().toChar()) {
                        (-1).toChar() -> throw IllegalArgumentException("Unfinished escape character")
                        'u' -> {
                            val buffer = CharArray(4)
                            reader.read(buffer)

                            Integer.parseInt(String(buffer), 16).toChar()
                        }
                        't' -> 0x9.toChar()
                        'r' -> 0xd.toChar()
                        'n' -> 0xa.toChar()
                        '\\' -> 0x5c.toChar()
                        '\"' -> 0x22.toChar()
                        else -> throw IllegalArgumentException("Unsupported escape character '\\$nextCh'")
                    }
                )
            } else if (ch == '\"') {
                break
            } else {
                tokenBuilder.append(ch)
            }
        }

        return EdnStringToken(tokenBuilder.toString())
    }

    private fun readSpecialWord(): EdnToken {
        while(true) {
            val ch = try {
                nextChar()
            } catch (_: NoSuchElementException) {
                break
            }

            if (ch.isEdnWhitespace) {
                break
            }

            if (!ch.isLetter()) {
                reader.unread(ch.code)
                break
            }

            tokenBuilder.append(ch)
        }

        return when(val token = tokenBuilder.toString()) {
            "true", "false" -> EdnBooleanToken(token)
            "nil" -> EdnNilToken(token)
            "NaN", "-Infinity", "Infinity" -> EdnDoubleToken(token)
            else -> throw IllegalArgumentException("Unrecognized EDN builtin '$token'")
        }
    }
}

val Char.isEdnWhitespace: Boolean
    get() = Character.isWhitespace(this) || this == ','

sealed interface EdnToken

@JvmInline
value class EdnSymbolToken(val value: String): EdnToken

@JvmInline
value class EdnKeywordToken(val value: String): EdnToken {
    init {
        when {
            value.isEmpty() || value == ":" -> throw IllegalArgumentException("Incomplete keyword '$value'")
            value[1] in '0'..'9' -> throw IllegalArgumentException("Keyword cannot start with a digit: '$value'")
            value[1] == ':' || value[1] == '/' || value[1] == '#' -> throw IllegalArgumentException("Keyword cannot start ${value.take(2)}: '$value'")
        }
    }
}

@JvmInline
value class EdnLongToken(val value: String): EdnToken

@JvmInline
value class EdnDoubleToken(val value: String): EdnToken {
    fun asDouble(): Double =
        when(value) {
            "NaN" -> Double.NaN
            "-Infinity" -> Double.NEGATIVE_INFINITY
            "Infinity" -> Double.POSITIVE_INFINITY
            else -> value.toDouble()
        }
}

@JvmInline
value class EdnTagToken(val value: String): EdnToken {
    init {
        when {
            !Character.isLetter(value[1]) -> throw IllegalArgumentException("Tags must have a letter following #: '$value'")
        }
    }
}

@JvmInline
value class EdnStringToken(val value: String): EdnToken

@JvmInline
value class EdnBooleanToken(val value: String): EdnToken

@JvmInline
value class EdnNilToken(val value: String): EdnToken