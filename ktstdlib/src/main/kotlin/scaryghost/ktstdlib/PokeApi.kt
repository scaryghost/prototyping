package scaryghost.ktstdlib

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import java.io.File
import java.io.OutputStreamWriter
import java.net.URI
import kotlin.math.ceil
import kotlin.math.floor

fun main(args: Array<String>): Unit = runBlocking{
    val portion = 1301f / 8f
    val results = (1 .. 8).map {
        val lower = ceil((it - 1) * portion).toInt() + 1
        val upper = floor(it * portion).toInt() + 1

        async {
            (lower .. upper).map { id ->
                downloadPokemonData(id)
            }
        }
    }.flatMap { it.await() }
    .iterator()

    File("pokemon.json").writer().use { writer ->
        writer.write("[")

        results.next().write(writer)
        while(results.hasNext()) {
            writer.write(",")
            results.next().write(writer)
        }

        writer.write("]")
    }
}

fun Result<String>.write(writer: OutputStreamWriter) {
    this.map {
        writer.write(it)
    }.onFailure {
        System.err.println("error retrieving pokemon data")
        it.printStackTrace(System.err)
    }
}
suspend fun downloadPokemonData(id: Int): Result<String> = withContext(Dispatchers.IO) {
    println("Downloading pokemon $id")

    kotlin.runCatching {
        val connection = URI.create("https://pokeapi.co/api/v2/pokemon/$id")
            .toURL()
            .openConnection()

        connection.connect()
        connection.inputStream.bufferedReader().use {
            it.readText()
        }
    }
}