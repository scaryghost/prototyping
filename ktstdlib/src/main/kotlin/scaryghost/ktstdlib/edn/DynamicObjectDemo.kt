package scaryghost.ktstdlib.edn

import com.github.rschmitt.dynamicobject.DynamicObject
import com.github.rschmitt.dynamicobject.EdnTranslator
import com.github.rschmitt.dynamicobject.Key
import scaryghost.ktstdlib.ktserialization.Armament.Placement
import scaryghost.ktstdlib.ktserialization.EDN
import java.time.Instant
import java.util.*
import java.util.Calendar.AUGUST

fun main(args: Array<String>) {
    DynamicObject.registerType(Placement::class.java, ArmamentPlacementTranslator)

    val ms = DynamicObject.newInstance(MobileSuit::class.java)
        .withName("""ZGMF-X19A 
            |∞ Justice""".trimMargin())
        .withPilots(
            setOf(
                DynamicObject.newInstance(Pilot::class.java)
                    .withFirstName("Athrun")
                    .withLastName("Zala"),
                DynamicObject.newInstance(Pilot::class.java)
                    .withFirstName("Lacus")
                    .withLastName("Clyne")
            )
        )
        .withDebut(
            Calendar.Builder()
                .setDate(2005, AUGUST, 6)
                .setTimeOfDay(18, 22, 20)
                .setTimeZone(TimeZone.getTimeZone("GMT+9"))
                .build()
                .toInstant()
        )
        .withOperationSystem("Generation Unsubdued Nuclear Drive Assault Module Complex")
        .withHeight(18.9f)
        .withWeight(79.67f)
        .withArmaments(
            listOf(
                4 to "MMI-GAU26 17.5mm CIWS",
                2 to "MMI-M19L 14mm Twin CIWS",
                2 to "MA-M02G \"Super Lacerta\" Beam Saber",
                1 to "MA-M1911 High-energy Beam Rifle",
                2 to "MR-Q15A \"Griffon\" Beam Blade",
                2 to "MA-6J \"Hyper Fortis\" Beam Cannon"
            ).map { (count, name) ->
                DynamicObject.newInstance(Armament::class.java)
                    .withName(name)
                    .withCount(count)
                    .withPlacement(Placement.FIXED)
            } + listOf(
                1 to "MA-M1911 High-energy Beam Rifle",
            ).map { (count, name) ->
                DynamicObject.newInstance(Armament::class.java)
                    .withName(name)
                    .withCount(count)
                    .withPlacement(Placement.HANDHELD)
            }
        )
        .withEquipment(
            listOf(
                DynamicObject.newInstance(Equipment::class.java)
                    .withName("Fatum-01",)
                    .withArmaments(
                        listOf(
                            1 to "MA-M02G \"Super Lacerta\" Beam Saber",
                            2 to "MA-6J \"Hyper Fortis\" Beam Cannon",
                            2 to "MA-M02S \"Preface Lacerta\" Beam Saber",
                            2 to "MR-Q17X \"Griffon 2\" Beam Blade"
                        ).flatMap { (count, name) ->
                            (0 ..< count).map {
                                DynamicObject.newInstance(Armament::class.java)
                                    .withName(name)
                            }
                        }
                    ),
                DynamicObject.newInstance(Equipment::class.java)
                    .withName("MX2002 Carrier Beam Shield")
                    .withArmaments(
                        listOf(
                            1 to "EEQ8 Grapple Stinger",
                            1 to "RQM55 \"Shining Edge\" Beam Boomerang",
                        ).flatMap { (count, name) ->
                            (0 ..< count).map {
                                DynamicObject.newInstance(Armament::class.java)
                                    .withName(name)
                            }
                        }
                    )
            )
        )
    val edn = DynamicObject.serialize(ms)

    ms.prettyPrint()
    println(edn)

    val copy = DynamicObject.deserialize(EDN, MobileSuit::class.java)

    println(copy == ms)


    val d = Instant.now().let {
        DynamicObject.newInstance(DynamicObjectDate::class.java)
            .withIndexedValues(listOf(it))
            .withTimestamp(it)
    }

    println("og ${d.indexedValues}")

    val d_copy = DynamicObject.deserialize(DynamicObject.serialize(d), DynamicObjectDate::class.java)
    println(d_copy.timestamp)
    println(d_copy.indexedValues)
    println(d_copy.indexedValues!![0])
}

interface DynamicObjectDate: DynamicObject<DynamicObjectDate> {
    @get:Key(":raw-instant") val timestamp: Instant
    @Key(":raw-instant") fun withTimestamp(value: Instant): DynamicObjectDate

    @get:Key(":indexed-values") val indexedValues: List<Any>?
    @Key(":indexed-values") fun withIndexedValues(values: List<Any>?): DynamicObjectDate
}

interface MobileSuit : DynamicObject<MobileSuit> {
    @Key(":name") fun getName(): String
    @Key(":name") fun withName(name: String): MobileSuit

    @Key(":pilots") fun getPilots(): Pilot
    @Key(":pilots") fun withPilots(pilots: Set<Pilot>): MobileSuit

    @Key(":debut") fun getDebut(): Instant
    @Key(":debut") fun withDebut(timestamp: Instant): MobileSuit

    @Key(":meteor-compatible") fun isMeteorCompatible(): Boolean
    @Key(":meteor-compatible") fun withMeteorCompatible(flat: Boolean): MobileSuit

    @Key(":armaments") fun getArmaments(): List<Armament>
    @Key(":armaments") fun withArmaments(armaments: List<Armament>): MobileSuit

    @Key(":equipment") fun getEquipment(): List<Equipment>
    @Key(":equipment") fun withEquipment(equipment: List<Equipment>): MobileSuit

    @Key(":height") fun getHeight(): Float
    @Key(":height") fun withHeight(height: Float): MobileSuit

    @Key(":weight") fun getWeight(): Float
    @Key(":weight") fun withWeight(weight: Float): MobileSuit

    @Key(":os") fun getOperationSystem(): String
    @Key(":os") fun withOperationSystem(os: String): MobileSuit
}

interface Equipment : DynamicObject<Equipment> {
    @Key(":name") fun getName(): String
    @Key(":name") fun withName(name: String): Equipment

    @Key(":armaments") fun getArmaments(): List<Armament>
    @Key(":armaments") fun withArmaments(armaments: List<Armament>): Equipment
}

interface Armament : DynamicObject<Armament> {
    @Key(":name") fun getName(): String
    @Key(":name") fun withName(name: String): Armament

    @Key(":count") fun getCount(): Int
    @Key(":count") fun withCount(count: Int): Armament

    @Key(":placement") fun getPlacement(): Placement
    @Key(":placement") fun withPlacement(placement: Placement): Armament
}

interface Pilot : DynamicObject<Pilot> {
    @Key(":last-name") fun getLastName(): String
    @Key(":last-name") fun withLastName(lastname: String): Pilot

    @Key(":first-name") fun getFirstName(): String
    @Key(":first-name") fun withFirstName(firstname: String): Pilot
}

object ArmamentPlacementTranslator : EdnTranslator<Placement> {
    override fun read(obj: Any?): Placement {
        return Placement.valueOf(obj.toString())
    }

    override fun write(obj: Placement?): String =
        when(obj) {
            null -> "nil"
            else -> " \"${obj.name}\""
        }

    override fun getTag(): String = Placement::class.qualifiedName!!

}