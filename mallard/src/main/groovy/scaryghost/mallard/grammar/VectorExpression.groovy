package scaryghost.mallard.grammar

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class VectorExpression implements Expression {
    List<Expression> expressions

    @Override
    String getFlattened() {
        return "[${expressions.collect {it.flattened }.join(', ')}]"
    }
}
