package scaryghost.mallard.grammar

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class Identifier implements Expression {
    String token

    @Override
    String getFlattened() {
        return token
    }
}
