package scaryghost.mallard.grammar

trait Expression extends Statement {
    abstract String getFlattened()
}
