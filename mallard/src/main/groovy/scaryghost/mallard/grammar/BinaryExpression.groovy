package scaryghost.mallard.grammar

import groovy.transform.AutoClone
import groovy.transform.EqualsAndHashCode

@AutoClone
@EqualsAndHashCode
class BinaryExpression implements Expression {
    Expression left, right
    String operator
    int priority

    @Override
    String getFlattened() {
        return "(${left.getFlattened()} $operator ${right.getFlattened()})"
    }
}
