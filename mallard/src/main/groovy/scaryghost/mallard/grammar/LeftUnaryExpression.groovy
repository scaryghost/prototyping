package scaryghost.mallard.grammar

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class LeftUnaryExpression implements Expression {
    Expression expr;
    String operator;

    @Override
    String getFlattened() {
        return "($operator${expr.getFlattened()})"
    }
}
