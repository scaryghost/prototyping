package scaryghost.mallard.grammar

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class Assignment implements Statement {
    String identifier
    Expression expression
}
