package scaryghost.mallard.parser

import groovy.transform.ToString
import scaryghost.mallard.grammar.BinaryExpression
import scaryghost.mallard.grammar.Expression
import scaryghost.mallard.grammar.Identifier
import scaryghost.mallard.grammar.LeftUnaryExpression
import scaryghost.mallard.grammar.Statement
import scaryghost.mallard.grammar.VectorExpression

import static scaryghost.mallard.parser.Operators.BINARY
import static scaryghost.mallard.parser.Operators.LEFT_UNARY

class ExpressionParser {
    static def IDENTIFIER_PATTERN = ~'^[a-z|A-Z|_]\\w*$'

    private Stack<State> states

    ExpressionParser() {
        states = [new State(munch: this.&munch_root)] as Stack
    }

    void munch(String token) {
        states.peek().munch(token)
    }

    Statement getStatement() {
        return states.peek().root
    }

    boolean getIsAccepted() {
        return states.size() == 1 && states.peek().munch.method === this.&munch_identifier.method
    }

    private void munch_root(String token) {
        def state = this.states.peek()

        switch(token) {
            case IDENTIFIER_PATTERN:
                state.root = new Identifier(token: token)
                state.current = state.root
                state.munch = this.&munch_identifier
                break
            case { LEFT_UNARY.contains(it) }:
                state.root = new LeftUnaryExpression(operator: token)
                state.current = state.root
                state.munch = this.&munch_leftUnaryExpression
                break
            case "(":
                states.push(new State(munch: this.&munch_root))
                break
            case "[":
                state.root = new VectorExpression(expressions: [])
                state.current = state.root
                states.push(new State(munch: this.&munch_root))
                break
            default:
                throw new IllegalArgumentException("Cannot start an expression with '$token'")
        }
    }

    private void munch_leftUnaryExpression(String token) {
        def state = this.states.peek()

        switch(token) {
            case IDENTIFIER_PATTERN:
                state.current.expr = new Identifier(token: token)
                state.munch = this.&munch_identifier
                break
            case { LEFT_UNARY.contains(it) }:
                state.current.expr = new LeftUnaryExpression(operator: token)
                state.current = state.current.expr
                break
            case "(":
                states.push(new State(munch: this.&munch_root))
                break
            default:
                throw new IllegalArgumentException("Cannot start an expression with '$token'")
        }
    }

    private void munch_identifier(String token) {
        switch (token) {
            case { BINARY.containsKey(it)}:
                def state = this.states.peek()
                def priority = BINARY[token]

                switch(state.current) {
                    case {it instanceof Identifier}:
                        state.root = new BinaryExpression(left: state.current, operator: token, priority: priority)
                        state.current = state.root
                        break
                    case {it instanceof BinaryExpression}:
                        if (state.current.priority < priority) {
                            state.current.right = new BinaryExpression(left: state.current.right, operator: token, priority: priority)
                            state.current = state.current.right
                        } else {
                            state.current.left = state.current.clone()
                            state.current.operator = token
                            state.current.priority = priority
                            state.current.right = null
                        }
                        break
                    default:
                        throw new IllegalStateException("Current type (${state.current.getClass()}) cannot be used with a binary operator")
                }

                state.munch = this.&munch_binaryExpression
                break
            case ")":
                if (states.size() == 1) {
                    throw new IllegalArgumentException("Unmatched ')'")
                }

                def completed = states.pop()
                def state = states.peek()

                if (completed.root instanceof BinaryExpression) {
                    completed.root.priority = Integer.MAX_VALUE
                }

                switch(state.current) {
                    case null:
                        state.root = completed.root
                        state.current = state.root
                        break
                    case {it instanceof LeftUnaryExpression}:
                        state.current.expr = completed.root
                        break
                    case {it instanceof BinaryExpression}:
                        if (state.current.left == null) {
                            state.current.left = completed.root
                        } else if (state.current.right == null) {
                            state.current.right = completed.root
                        } else {
                            throw new IllegalStateException("Unable to assign grouped expression to its parent binary expression")
                        }
                        break
                }
                state.munch = this.&munch_identifier
                break
            case ",":
                if (states.size() == 1) {
                    throw new IllegalArgumentException("Not currently building a vector, set, or map")
                }

                def current = states.pop()
                def vectorState = states.peek()

                if (current.munch.method === this.&munch_identifier.method) {
                    vectorState.root.expressions << current.root
                } else {
                    throw new IllegalStateException("Expression is incomplete")
                }

                vectorState.munch = this.&munch_root
                states.push(new State(munch: this.&munch_root))
                break
            case "]":
                if (states.size() == 1) {
                    throw new IllegalArgumentException("Not currently building a vector, set, or map")
                }

                def current = states.pop()
                def vectorState = states.peek()

                if (current.munch.method === this.&munch_identifier.method) {
                    vectorState.root.expressions << current.root
                } else {
                    throw new IllegalStateException("Expression is incomplete")
                }

                vectorState.munch = this.&munch_identifier
                break
            default:
                throw new IllegalArgumentException("Need a binary operator after an identifier")
        }
    }

    private void munch_binaryExpression(String token) {
        def state = this.states.peek()

        switch(token) {
            case IDENTIFIER_PATTERN:
                state.current.right = new Identifier(token: token)
                state.munch = this.&munch_identifier
                break
            case { LEFT_UNARY.contains(it) }:
                state.current.right = new LeftUnaryExpression(operator: token)
                state.current = state.current.right
                state.munch = this.&munch_leftUnaryExpression
                break
            case "(":
                states.push(new State(munch: this.&munch_root))
                break
            default:
                throw new IllegalArgumentException("Need an identifier or a left unary operator after a binary operator")
        }
    }

    @ToString
    private static class State {
        Expression root, current
        def munch
    }
}
