package scaryghost.mallard.parser

import scaryghost.mallard.grammar.Assignment
import scaryghost.mallard.grammar.Statement

class AssignmentParser {
    private def expressionParser = new ExpressionParser()
    private def identifier
    def munch = this.&munch_root

    boolean getIsAccepted() {
        return expressionParser.getIsAccepted()
    }

    Statement getStatement() {
        return new Assignment(identifier: identifier, expression: expressionParser.statement)
    }

    private void munch_root(String token) {
        switch(token) {
            case "val":
                munch = this.&munch_identifier
                break
            default:
                throw new IllegalArgumentException("Assignment must begin with 'val'")
        }
    }

    private void munch_identifier(String token) {
        switch(token) {
            case ExpressionParser.IDENTIFIER_PATTERN:
                identifier = token
                munch = this.&munch_equals
                break
            default:
                throw new IllegalArgumentException("Invalid identifier: '$token'")
        }
    }

    private void munch_equals(String token) {
        switch(token) {
            case "=":
                munch = expressionParser.&munch
                break
            default:
                throw new IllegalArgumentException("Missing '=' after identifier")
        }
    }
}
