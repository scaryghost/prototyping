package scaryghost.mallard.parser

class Operators {
    public static Map<String, Integer> BINARY = [
        "+": 0, "-": 0, "*": 1, "/": 1
    ]
    public static Set<String> LEFT_UNARY = ["-"]
}
