package scaryghost.mallard

import scaryghost.mallard.grammar.Statement
import scaryghost.mallard.parser.AssignmentParser
import scaryghost.mallard.parser.ExpressionParser

class Interpreter {
    Tokenizer tokenizer
    private def lastToken

    Statement next() {
        def possible = [new AssignmentParser(), new ExpressionParser()]

        while (true) {
            def token = nextToken()

            if (token != null) {
                def validParsers = possible.findAll {
                    try {
                        it.munch(token)
                        true
                    } catch (Exception ignored) {
                        false
                    }
                }

                if (validParsers.size() == 0) {
                    def accepted = possible.findAll {
                        it.isAccepted
                    }

                    if (accepted.size() == 1) {
                        lastToken = token
                        return accepted[0].statement
                    }

                    throw new IllegalArgumentException("Ambiguous statement, unsure how to handle '$token'")
                }

                possible = validParsers
            } else {
                switch(possible.size()) {
                    case 0:
                        throw new IllegalStateException("Unrecognized statement")
                    case 1:
                        if (possible[0].isAccepted) {
                            return possible[0].statement
                        }
                        throw new IllegalStateException("Invalid statement")
                    default:
                        throw new IllegalArgumentException("Ambiguous statement")
                }
            }
        }
    }

    private def nextToken() {
        def value
        if (lastToken != null) {
            value = lastToken
            lastToken = null
        } else {
            value = tokenizer.next()
        }

        return value
    }
}
