package scaryghost.mallard


import scaryghost.mallard.recognizer.OneOf

import static scaryghost.mallard.recognizer.DFAs.*

class Tokenizer {
    Reader reader
    private def lastCh

    String next() {
        def recognizers = new OneOf(
            possible: [integerLiteral(), doubleLiteral(), stringLiteral(), identifier(), operators(), delimiters()]
        )

        def builder = new StringBuilder()
        def munched = false
        char ch

        while((ch = nextChar()) != -1) {
            builder.append(ch)
            if (munched || !Character.isWhitespace(ch)) {
                munched = true
                try {
                    recognizers.munch(ch)
                } catch (IllegalStateException | IllegalArgumentException e) {
                    if (recognizers.hasAccepted) {
                        lastCh = ch
                        break
                    } else {
                        throw e
                    }
                }
            }
        }

        if (!munched) {
            return null
        }

        if (!recognizers.hasAccepted) {
            throw new IllegalStateException("Current token is not a valid literal nor text: '${builder.toString()}'")
        }

        return recognizers.capture
    }

    private int nextChar() {
        def value

        if (lastCh != null) {
            value = lastCh
            lastCh = null
        } else {
            value = reader.read()
        }

        return value
    }
}