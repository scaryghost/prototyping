package scaryghost.mallard.recognizer


class DFAs {
    static DFA integerLiteral() {
        return new DFA([
            new DFA.State(transitionPredicates: [(Character.&isDigit): 1]),
            new DFA.AcceptingState(transitionPredicates: [(Character.&isDigit): 1]),
        ])
    }

    static DFA doubleLiteral() {
        return new DFA([
            new DFA.State(transitionPredicates: [(Character.&isDigit): 1]),
            new DFA.State(transitions: [((char) '.'): 2], transitionPredicates: [(Character.&isDigit): 1]),
            new DFA.State(transitionPredicates: [(Character.&isDigit): 3]),
            new DFA.AcceptingState(transitionPredicates: [(Character.&isDigit): 3])
        ])
    }

    static DFA stringLiteral() {
        return new DFA([
            new DFA.State(transitions: [((char) '"'): 1]),
            new DFA.State(recordChar: false, transitions: [((char) '"'): 3], transitionPredicates: [{ it != (char) '"'}: 2]),
            new DFA.State(transitions: [((char) '"'): 3], transitionPredicates: [{ it != (char) '"'}: 2]),
            new DFA.AcceptingState(recordChar: false, transitions: NONE)
        ])
    }

    static DFA identifier() {
        return new DFA([
            new DFA.State(transitionPredicates: [(Character.&isLetter): 1, { it == (char) '_' }: 1]),
            new DFA.AcceptingState(transitionPredicates: [(Character.&isLetterOrDigit): 1, { it == (char) '_' }: 1])
        ])
    }

    static DFA operators() {
        def acceptChar = { ch ->
            new DFA.AcceptingState(transitions: [((char) ch): 1])
        }
        return new DFA([
            new DFA.State(transitions: [
                ((char) '*'): 2,
                ((char) '&'): 3,
                ((char) '|'): 4,
                ((char) '!'): 5,
                ((char) '='): 6,
                ((char) '?'): 7,
                ((char) '<'): 8,
                ((char) '>'): 9,
                ((char) ':'): 10,
                ((char) '+'): 1,
                ((char) '-'): 1,
                ((char) '/'): 1,
                ((char) '~'): 1,
                ((char) '.'): 1

            ]),
            new DFA.AcceptingState(transitions: NONE),
            acceptChar('*'),
            acceptChar('&'),
            acceptChar('|'),
            acceptChar('!'),
            acceptChar('='),
            acceptChar('.'),
            acceptChar('<'),
            acceptChar('>'),
            acceptChar(':')
        ])
    }

    static DFA delimiters() {
        return new DFA([
            new DFA.State(transitions: [
                ((char) ','): 1,
                ((char) '('): 1,
                ((char) ')'): 1,
                ((char) '['): 1,
                ((char) ']'): 1,
                ((char) '{'): 1,
                ((char) '}'): 1,
                ((char) ';'): 1
            ], transitionPredicates: [(Character.&isWhitespace): 1]),
            new DFA.AcceptingState(transitions: NONE)
        ])
    }

    private static Map<Character, Integer> NONE = Collections.<Character, Integer>emptyMap()
}
