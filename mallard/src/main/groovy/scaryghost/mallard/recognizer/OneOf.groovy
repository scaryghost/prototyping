package scaryghost.mallard.recognizer

class OneOf implements Automata {
    List<Automata> possible

    @Override
    Automata munch(char token) {
        def valid = possible.findAll {
            try {
                it.munch(token)
                true
            } catch (IllegalArgumentException | IllegalStateException ignored) {
                false
            }
        }

        if (valid.size() == 0) {
            throw new IllegalArgumentException("Given an invalid token: '$token'")
        }

        possible = valid
        return this
    }

    @Override
    boolean getHasAccepted() {
        return possible.findAll { it.hasAccepted }.size() == 1
    }

    @Override
    String getCapture() {
        return hasAccepted ? possible[0].capture : null
    }
}
