package scaryghost.mallard.recognizer

class DFA implements Automata {
    List<State> states
    private State current
    private StringBuilder _capture

    DFA(List<State> states) {
        this.states = states
        current = states[0]
        _capture = new StringBuilder()
    }

    @Override
    DFA munch(char token) {
        def transition = {
            current = states[it]
            if (current.recordChar) {
                _capture.append(token)
            }
        }

        if (current.transitions.containsKey(token)) {
            transition(current.transitions[token])
            return this
        }

        def nextStates = current.transitionPredicates
            .findAll { it.key.call(token) }
            .collect { it.value}

        if (nextStates.size() > 1) {
            throw new IllegalStateException("'$token' has multiple valid transitions: $nextStates")
        }
        if (nextStates.size() != 0) {
            transition(nextStates[0])
            return this
        }

        throw new IllegalArgumentException("Given an invalid token: '$token'")
    }

    @Override
    String getCapture() {
        return hasAccepted ? _capture.toString() : null
    }

    @Override
    boolean getHasAccepted() {
        return current instanceof AcceptingState
    }

    static class State {
        Map<Character, Integer> transitions = [:]
        Map<Closure<Boolean>, Integer> transitionPredicates = [:]
        boolean recordChar = true
    }

    static class AcceptingState extends State { }
}
