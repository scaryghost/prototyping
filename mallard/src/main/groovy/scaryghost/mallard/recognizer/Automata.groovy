package scaryghost.mallard.recognizer

interface Automata {
    Automata munch(char token)
    boolean getHasAccepted()
    String getCapture()
}