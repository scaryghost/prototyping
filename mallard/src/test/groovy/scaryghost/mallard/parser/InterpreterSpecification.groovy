package scaryghost.mallard.parser

import scaryghost.mallard.Interpreter
import scaryghost.mallard.grammar.Assignment
import scaryghost.mallard.grammar.BinaryExpression
import scaryghost.mallard.grammar.Identifier
import scaryghost.mallard.grammar.VectorExpression
import spock.lang.Specification

import static scaryghost.mallard.parser.TokenizerSpecification.setupTokenizer

class InterpreterSpecification extends Specification {
    def 'assignment'() {
        given:
        def input =
"""
val sum = x + y
val delta = sum - expected
delta
"""
        def interpreter = new Interpreter(tokenizer: setupTokenizer(input))

        when:
        def statement1 = interpreter.next()
        def statement2 = interpreter.next()
        def statement3 = interpreter.next()

        then:
        statement1 == new Assignment(
            identifier: "sum",
            expression: new BinaryExpression(
                left: new Identifier(token: "x"),
                right: new Identifier(token: "y"),
                operator: "+",
                priority: 0
            )
        )

        statement2 == new Assignment(
            identifier: "delta",
            expression: new BinaryExpression(
                left: new Identifier(token: "sum"),
                right: new Identifier(token: "expected"),
                operator: "-",
                priority: 0
            )
        )

        statement3 == new Identifier(token: "delta")
    }

    def 'vector'() {
        given:
        def input = "[x, [y, x - (y - z)], [w]]"
        def interpreter = new Interpreter(tokenizer: setupTokenizer(input))

        when:
        def vector = interpreter.next()

        then:
        vector == new VectorExpression(expressions: [
            new Identifier(token: "x"),
            new VectorExpression(expressions: [
                new Identifier(token: "y"),
                new BinaryExpression(
                    left: new Identifier(token: "x"),
                    right: new BinaryExpression(
                        left: new Identifier(token: "y"),
                        right: new Identifier(token: "z"),
                        operator: "-",
                        priority: Integer.MAX_VALUE
                    ),
                    operator: "-",
                    priority: 0
                )
            ]),
            new VectorExpression(expressions: [
                new Identifier(token: "w")
            ])
        ])
    }
}
