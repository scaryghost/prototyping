package scaryghost.mallard.parser

import scaryghost.mallard.Tokenizer
import spock.lang.Specification

class TokenizerSpecification extends Specification {
    def "good tokens"(msg, expected) {
        given:
        def tokenizer = setupTokenizer(msg)

        when:
        def tokens = collectTokens(tokenizer)

        then:
        tokens == expected

        where:
        msg | expected
        "3 14\t159\n2653" | ["3", "14", "159", "2653"]
        "2.0 71.828\t182.84590\n4523.53602" | ["2.0", "71.828", "182.84590", "4523.53602"]
        "x _private_field\tcamelCase\nv1" | ["x", "_private_field", "camelCase", "v1"]
        "\"the quick brown fox \" \"jumps over \"\t\"the lazy dog\"\n\"!@#\$%^&*()\"" | [
            "the quick brown fox ", "jumps over ", "the lazy dog", "!@#\$%^&*()"
        ]
    }

    def "unfinished tokens"(msg) {
        given:
        def tokenizer = setupTokenizer(msg)

        when:
        collectTokens(tokenizer)

        then:
        thrown(IllegalStateException)

        where:
        msg | _
        "3." | _
        "\"unclosed quote" | _
    }

    def "delimiters"(msg, expected) {
        given:
        def tokenizer = setupTokenizer(msg)

        when:
        def tokens = collectTokens(tokenizer)

        then:
        tokens == expected

        where:
        msg | expected
        "x+(y - z ) /w" | ["x", "+", "(", "y", "-", "z", ")", "/", "w"]
        "array[0]" | ["array", "[", "0", "]"]
        "--1.7320" | ["-", "-", "1.7320"]
        "{135, \"Jolteon\", \"Quick Feet\"}" | ["{", "135", ",", "Jolteon", ",", "Quick Feet", "}"]
        "a=b" | ["a", "=", "b"]
    }

    static def setupTokenizer(msg) {
        return new Tokenizer(reader: new StringReader(msg))
    }

    private static def collectTokens(tokenizer) {
        def tokens = []
        def t

        while((t = tokenizer.next()) != null) {
            tokens << t
        }
        return tokens
    }
}
