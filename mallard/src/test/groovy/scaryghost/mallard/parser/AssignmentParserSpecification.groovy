package scaryghost.mallard.parser

import spock.lang.Specification

class AssignmentParserSpecification extends Specification {
    def "valid"(tokens, expectedIdentifier, expectedExpression) {
        given:
        def parser = new AssignmentParser()

        when:
        tokens.each{ parser.munch(it) }

        then:
        parser.isAccepted
        parser.statement.identifier == expectedIdentifier
        parser.statement.expression.flattened == expectedExpression

        where:
        tokens | expectedIdentifier | expectedExpression
        ["val", "sum", "=", "x", "+", "y"] | "sum" | "(x + y)"
    }

}
