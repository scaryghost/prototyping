package scaryghost.mallard.parser

import spock.lang.Specification

class ExpressionParserSpecification extends Specification {
    def "acceptedTokens"(tokens, expected) {
        given:
        def parser = new ExpressionParser()

        when:
        tokens.each{ parser.munch(it) }

        then:
        parser.isAccepted
        parser.statement.flattened == expected

        where:
        tokens | expected
        ["x"]  | "x"
        ["-", "x"] | "(-x)"
        ["-", "-", "y"] | "(-(-y))"
        ["x", "+", "y"] | "(x + y)"
        ["x", "-", "-", "y"] | "(x - (-y))"
        ["x", "+", "y", "*", "z"] | "(x + (y * z))"
        ["x", "+", "y", "+", "z", "+", "w"] | "(((x + y) + z) + w)"
        ["x", "+", "y", "*", "z", "/", "w"] | "(x + ((y * z) / w))"
        ["x", "*", "(", "y", "+", "z", ")", "/", "w"] | "((x * (y + z)) / w)"
        ["(", "x", "+", "y", ")", "*", "z"] | "((x + y) * z)"
        ["x", "+", "(", "y", "+", "z", ")"] | "(x + (y + z))"
        ["x", "*", "-", "(", "y", "+", "z", ")"] | "(x * (-(y + z)))"
    }
}
