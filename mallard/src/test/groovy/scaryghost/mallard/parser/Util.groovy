package scaryghost.mallard.parser

class Util {
    static ExpressionParser munchTokens(parser, tokens) {
        tokens.each{ parser.munch(it) }
        return parser
    }
}
